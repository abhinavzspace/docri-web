/**
 * Created by abhinav on 25/12/16.
 */

// REFERENCE: https://github.com/gajus/redux-immutable/blob/master/README.md#using-with-react-router-redux
import Immutable from 'immutable';
import {
    LOCATION_CHANGE
} from 'react-router-redux';

const initialState = Immutable.fromJS({
    locationBeforeTransitions: null
});

export default (state = initialState, action) => {
    if (action.type === LOCATION_CHANGE) {
        return state.set('locationBeforeTransitions', action.payload);
    }

    return state;
};
