import { combineReducers } from 'redux-immutable'
// import locationReducer from './location'
import locationRouterRedux from './location-router-redux'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    // location: locationReducer,
      // REFERENCE: https://github.com/reactjs/react-router-redux
      routing: locationRouterRedux,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
