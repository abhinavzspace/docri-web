import { applyMiddleware, compose, createStore } from 'redux'
// import thunk from 'redux-thunk'
import { browserHistory } from 'react-router'
import makeRootReducer from './reducers'
// import { updateLocation } from './location'
import {fromJS} from 'immutable'
import { createEpicMiddleware } from 'redux-observable';
import rootEpic from '../routes/DocriApp/rootEpic'      // FIXME: enable adding of epics asynchonously
import { routerMiddleware } from 'react-router-redux'

export default (initialState = fromJS({})) => {
  // ======================================================
  // Middleware Configuration
  // ======================================================
  const epicMiddleware = createEpicMiddleware(rootEpic)
  const middleware = [epicMiddleware, routerMiddleware(browserHistory)]

  // ======================================================
  // Store Enhancers
  // ======================================================
  const enhancers = []

  let composeEnhancers = compose

  if (__DEV__) {
    const composeWithDevToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    if (typeof composeWithDevToolsExtension === 'function') {
      composeEnhancers = composeWithDevToolsExtension
    }
  }

  // ======================================================
  // Store Instantiation and HMR Setup
  // ======================================================
  const store = createStore(
    makeRootReducer(),
    initialState,
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers
    )
  )

  // REFERENCE: https://github.com/redux-observable/redux-observable/issues/81
  if (window.devToolsExtension && __DEV__) {
      window.devToolsExtension.updateStore(store);
  }

  store.asyncReducers = {}

  // REMOVED
  // To unsubscribe, invoke `store.unsubscribeHistory()` anytime
  // store.unsubscribeHistory = browserHistory.listen(updateLocation(store))

  if (module.hot) {
    module.hot.accept('./reducers', () => {
      const reducers = require('./reducers').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}
