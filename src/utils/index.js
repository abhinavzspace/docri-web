/**
 * Created by abhinav on 15/10/16.
 */


export const getFirstObject = (obj) => {
    const firstKey = Object.keys(obj)[0]
    return obj[firstKey]
}
