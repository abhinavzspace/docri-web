/**
 * Created by abhinav on 16/12/16.
 */
let data = {}

export const setItem = (key, value) => {
    data[key] = value
    return value
}

export const getItem = (key) => {
    if (data[key]) return data[key]
    else throw `key: ${key} does not exist in inMemoryData`
}

export const removeItem = (key) => {
    if (data[key]) delete data[key]
}

export const hasItem = (key) => {
    return data[key] ? true : false
}

export const removeAll = () => {
    data = {}
}
