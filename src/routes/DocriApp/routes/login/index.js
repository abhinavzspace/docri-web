/**
 * Created by abhinav on 25/12/16.
 */
import {injectReducer} from '../../../../store/reducers'
import {getToken} from '../../containers/utils/connectors'
import {tryLoginTokenPresent} from '../../containers/Auth/actionCreators'
import {selectAuthStatus} from '../../containers/Auth/selectors'
import {asyncReducers, ADD_REDUCER} from '../../asyncReducerLogic'
import C from '../../containers/Auth/constants'
import {getUsername} from '../../containers/utils/connectors'

export default (store) => ({
    path: '/login',
    /*  Async getComponent is only invoked when route matches   */
    getComponent (nextState, cb) {
        /*  Webpack - use 'require.ensure' to create a split point
         and embed an async module loader (jsonp) when bundling   */
        require.ensure([], (require) => {
            /*  Webpack - use require callback to define
             dependencies for bundling   */

            const Auth = require('../../containers/Auth').default
            const authReducer = require('../../containers/Auth/reducer').default

            /*  Add the reducer to the store on key 'docri'  */
            injectReducer(store, {key: 'auth', reducer: authReducer})

            console.log(selectAuthStatus(store.getState()))
            if (getToken() && selectAuthStatus(store.getState()) !== 'AUTH_LOGGED_IN') {
                const url = '/' + getUsername()
                const actionToFire = {type: C.CHANGE_URL, url}
                store.dispatch(tryLoginTokenPresent(actionToFire))       // afterLogin goTo url /username
            }

            /*  Return getComponent   */
            cb(null, Auth)

            /* Webpack named bundle   */
        }, 'auth')
    }
})
