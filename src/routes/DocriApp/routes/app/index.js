/**
 * Created by abhinav on 12/10/16.
 */
import {injectReducer} from '../../../../store/reducers'
import {getToken} from '../../containers/utils/connectors'
// import {tryLoginTokenPresent} from '../../containers/Auth/actionCreators'
import {selectAuth} from '../../containers/Auth/selectors'
import {SET_ROUTE_PARAMS} from '../../containers/App/reducer'
import {DOCRI, FOLDER, NOTE, USERNAME} from '../../containers/DEPRECATED/Reaction/constants'
import {asyncReducers, ADD_REDUCER} from '../../asyncReducerLogic'
import {selectAuthStatus} from '../../containers/Auth/selectors'
import {ITEM_ROUTE_CHANGE} from '../../containers/ItemsTree/constants'
import {fetchTreeTokenPresent} from '../../containers/ItemsTree/actionCreators'

export default (store) => ({
    path: ':username',
    /*  Async getComponent is only invoked when route matches   */
    getComponent (nextState, cb) {
        /*  Webpack - use 'require.ensure' to create a split point
         and embed an async module loader (jsonp) when bundling   */
        require.ensure([], (require) => {
            /*  Webpack - use require callback to define
             dependencies for bundling   */

            const App = require('../../containers/App').default

            /*  Return getComponent   */
            cb(null, App)

            /* Webpack named bundle   */
        }, 'user')
    },

    onEnter: (nextState, replace) => {
        // if token not present, send to login page.
        if (!getToken()) replace('/login')
    },

    getChildRoutes(location, cb) {

        require.ensure([], (require) => {

            const {combinedReducer} = require('./reducers')
            const authReducer = require('../../containers/Auth/reducer').default

            /*  Add the reducer to the store on key 'docri'  */
            injectReducer(store, {key: 'user', reducer: combinedReducer})

            if (!store.getState().has('auth')) {
                injectReducer(store, {key: 'auth', reducer: authReducer})
            }

            asyncReducers.on(ADD_REDUCER, (name, newReducer) => {
                injectReducer(store, {key: name, reducer: newReducer})
            })

            // do asynchronous stuff to find the child routes
            cb(null, [getDocriesRoute(store), getFoldersRoute(store), getNotesRoute(store)])
        }, 'user-child')
    },

    getIndexRoute(partialNextState, cb) {

        require.ensure([], (require) => {

            // FIXME: REPEATED CODE

            const {combinedReducer} = require('./reducers')
            const authReducer = require('../../containers/Auth/reducer').default

            // FIXME: ADD CHECK
            /*  Add the reducer to the store on key 'docri'  */
            injectReducer(store, {key: 'user', reducer: combinedReducer})

            if (!store.getState().has('auth')) {
                injectReducer(store, {key: 'auth', reducer: authReducer})
            }

            asyncReducers.on(ADD_REDUCER, (name, newReducer) => {
                injectReducer(store, {key: name, reducer: newReducer})
            })

            // do something async here
            cb(null, getUsernameRoute(store))
        }, 'user-index')
    }
})

const getUsernameRoute = (store) => ({
    onEnter: setUrlParams(store, USERNAME)
})

const getDocriesRoute = (store) => ({
    path: 'docri/:id',
    onEnter: setUrlParams(store, DOCRI)
})

const getFoldersRoute = (store) => ({
    path: 'folder/:id',
    onEnter: setUrlParams(store, FOLDER)
})

const getNotesRoute = (store) => ({
    path: 'note/:id',
    onEnter: setUrlParams(store, NOTE)
})


const setUrlParams = (store, item) => (nextState) => {
    console.log('nextState', nextState, {item})
    const {username, id} = nextState.params

    let urlParams = {}
    urlParams['username'] = username
    urlParams['item'] = item
    if (item !== USERNAME) {
        urlParams['id'] = id
    }

    store.dispatch({type: SET_ROUTE_PARAMS, urlParams})         // synchronous action

    tryLoginAndInit(store)
}

const tryLoginAndInit = (store) => {
    const token = getToken()
    const authStatus = selectAuthStatus(store.getState())
    if (token && authStatus !== 'AUTH_LOGGED_IN') {
        // const actionToFire = {type: ITEM_ROUTE_CHANGE}
        store.dispatch(fetchTreeTokenPresent()) // afterLogin fire ITEM_ROUTE_CHANGE action
        // store.dispatch(actionToFire)
    } else if (token && authStatus === 'AUTH_LOGGED_IN') {
        store.dispatch({type: ITEM_ROUTE_CHANGE})
    }
}
