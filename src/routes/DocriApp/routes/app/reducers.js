/**
 * Created by abhinav on 12/10/16.
 */
import {combineReducers} from 'redux-immutable'
import appReducer from '../../containers/App/reducer'
// import docriesReducer from './containers/Docries/reducer'
// import authReducer from './containers/Auth/reducer'
import itemsReducer from '../../containers/ItemsTree/reducer'
// import foldersReducer from './containers/Folders/reducer'
// import notesReducer from './containers/Notes/reducer'

export const combinedReducer = combineReducers({
    app: appReducer,
    // auth: authReducer,
    items: itemsReducer
    // docries: docriesReducer,
    // folders: foldersReducer,
    // notes: notesReducer
})
