/**
 * Created by abhinav on 15/10/16.
 */

import React from 'react'

const Sidebar = ({children, isOpen, toggleSidebar}) => {
    let sidebarClass = isOpen ? 'sidebar open' : 'sidebar';
    return (
        <div className={sidebarClass}>

            <button onClick={toggleSidebar} className="sidebar-toggle">Toggle Sidebar</button>
            {children}
        </div>
    )
}

export default Sidebar
