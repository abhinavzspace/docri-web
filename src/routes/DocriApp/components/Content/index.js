/**
 * Created by abhinav on 15/10/16.
 */

import React from 'react'

const Content = ({isOpen}) => {
    let contentClass = isOpen ? 'content open' : 'content'
    return (
        <div className={contentClass}><p>This template has a responsive menu toggling system. The menu will appear
            collapsed on smaller screens, and will appear non-collapsed on larger screens. When toggled using the
            button below, the menu will appear/disappear. On small screens, the page content will be pushed off
            canvas.</p></div>
    )
}


export default Content
