/**
 * Created by abhinav on 15/10/16.
 */

import React from 'react'

const Header = ({onClick}) => {
    return (
        <header>
            <a href="javascript:;" onClick={onClick}>Click Me!</a>
        </header>
    )
}

export default Header
