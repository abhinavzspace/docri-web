/**
 * Created by abhinav on 21/12/16.
 */

export const localStorageGetItem = (key) => localStorage.getItem(key)

export const localStorageRemoveItem = (key) => localStorage.removeItem(key)

export const localStorageSetItem = (key, item) => localStorage.setItem(key, item)
