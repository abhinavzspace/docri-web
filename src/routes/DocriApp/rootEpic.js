/**
 * Created by abhinav on 30/11/16.
 */

import {combineEpics} from 'redux-observable'

import authEpics from './containers/Auth/epics'
// import docriesEpics from './containers/Docries/epics'
// import foldersEpics from './containers/Folders/epics'
// import notesEpics from './containers/Notes/epics'
import itemsEpics from './containers/ItemsTree/epics'


const rootEpic = combineEpics(
    authEpics,
    itemsEpics
    // docriesEpics,
    // foldersEpics,
    // notesEpics
)

export default rootEpic
