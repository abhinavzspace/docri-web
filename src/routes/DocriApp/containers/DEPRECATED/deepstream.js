/**
 * Created by abhinav on 03/12/16.
 */
import {Observable} from 'rxjs'
import {getDs} from '../utils/connectors'

export const observeRecord = (id) =>
    Observable.create(o => {
        const ds = getDs()
        const rec = ds.record.getRecord(id)
        const next = val => o.next(val)
        const error = err => {
            console.log('<------- observeRecord ERROR ------>', err);
            if (err.event === 'MESSAGE_DENIED') {
                console.log('MESSAGE DENIED for Record = ', err.error[1])
            } else {
                o.error(err)
            }
        }
        rec.subscribe(next, true)
        rec.on('error', error)
        return () => {
            rec.unsubscribe(next)
            rec.off('error', error)
            rec.discard()
        }
    })

// Example usage:
//     observeList('entry-added', list)
//         .mergeMap(([entry, position]) =>
export const observeList = (on, list) =>
    Observable.create(o => {
        const next = (...args) => o.next(args)
        list.on(on, next)
        return () => {
            list.off(on, next)
        }
    })


export const createList = (listId) =>
    // list becomes "ready" only after whenReady method
    Observable.create(o => {
        const ds = getDs()
        const list = ds.record.getList(listId)
        list.whenReady(l => o.next(l))
        return () => {
            list.discard()
        }
    })


export const getRecordDataOnce = (recordId) =>
    Observable.create(o => {
        const ds = getDs()
        ds.record.snapshot(recordId, (err, data) => {
            if (err) o.error(err)
            else o.next(data)
        })
    })
