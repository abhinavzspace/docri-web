/**
 * Created by abhinav on 23/12/16.
 */

export const USERNAME = 'USERNAME'
export const DOCRI = 'd'
export const FOLDER = 'f'
export const NOTE = 'n'

export const DOCRI_COLL = 'dc'
export const FOLDER_COLL = 'fc'
export const NOTE_COLL = 'nc'
