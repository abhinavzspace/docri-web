/**
 * Created by abhinav on 23/12/16.
 */
import {Observable} from 'rxjs'
import {USERNAME, DOCRI, FOLDER, NOTE, DOCRI_COLL, FOLDER_COLL, NOTE_COLL} from './constants'
import {observeRecord, createList, observeList, getRecordDataOnce} from '../utils/deepstream'
import {replaceEntries, changeEntry, changeSelectedEntry} from '../Items/actionCreators'
import {selectSelectedItem} from '../Items/selectors'
import {getReducerKey} from '../Items/actionCreators'
import {getUsername} from '../utils/connectors'


// Get Id to query deepstream
const getDsId = (id, type) => {
    let prefix
    switch (type) {
        case DOCRI:
            prefix = 'd/'
            break
        case FOLDER:
            prefix = 'f/'
            break
        case NOTE:
            prefix = 'n/'
            break
        case DOCRI_COLL:
            prefix = 'dc/'
            break
        case FOLDER_COLL:
            prefix = 'fc/'
            break
        case NOTE_COLL:
            prefix = 'nc/'
            break
        default:
            throw new Error('This type does not exist, type = ', type)
    }
    return prefix + id
}


// DOCRI_COLL ---> DOCRI,
// FOLDER_COLL ---> FOLDER,
// etc
const collTypeToBasicType = (collType) => {
    switch (collType) {
        case DOCRI_COLL:
            return DOCRI
        case FOLDER_COLL:
            return FOLDER
        case NOTE_COLL:
            return NOTE
        default:
            throw new Error('This collType is not available. collType = ', collType)
    }
}


// returns Observable
// IMPORTANT: id should be pure, i.e. only "uuid"
export const reaction = (id, type, getState, onlyDown) => {
    console.log(id, type)
    const dsId = getDsId(id, type)
    switch (type) {
        case USERNAME:
        case DOCRI:
        case FOLDER:
        case NOTE:
            return recordReaction(id, dsId, type, getState, onlyDown)
        case DOCRI_COLL:
        case FOLDER_COLL:
        case NOTE_COLL:
            return listReaction(id, dsId, type, getState)
        default:
            throw new Error(type + ' type does not exist.')
    }
}

const DATA_INSUFFICIENT = 'Record data does not have sufficient ids.'


// if onlyDown is defined and true, dont go up in the hierarchy
const recordReaction = (id, dsId, type, getState, onlyDown) =>
    // observeRecord(dsId)
    getRecordDataOnce(dsId)
        .mergeMap(data => {
            let obs = []
            switch (type) {
                case USERNAME: // NOT IN USE
                    // const {dcId} = data
                    // if (!dcId) throw new Error(DATA_INSUFFICIENT)
                    // return reaction(dcId, DOCRI_COLL, getState)

                // FALL THROUGH
                case DOCRI:

                    obs = [
                        Observable.of(changeSelectedEntry(type, id, data)),      // dispatches
                        reaction(id, FOLDER_COLL, getState)     // d/uuid  -->  fc/uuid
                    ]
                    if (!onlyDown)
                        obs.push(reaction(getUsername(), DOCRI_COLL, getState))
                    return Observable.merge(
                        ...obs
                    )

                case FOLDER:
                    const {dId} = data
                    if (!dId) throw new Error(DATA_INSUFFICIENT)

                    obs = [
                        Observable.of(changeSelectedEntry(type, id, data)),      // dispatches
                        reaction(id, NOTE_COLL, getState)       // f/uuid  -->  nc/uuid
                    ]
                    if (!onlyDown)
                        obs.push(reaction(dId, DOCRI, getState))
                    return Observable.merge(
                        ...obs
                    )

                case NOTE:
                    const {fId} = data
                    if (!fId) throw new Error(DATA_INSUFFICIENT)

                    obs = [
                        Observable.of(changeSelectedEntry(type, id, data))        // dispatches
                    ]
                    if (!onlyDown)
                        obs.push(reaction(fId, FOLDER, getState))
                    return Observable.merge(
                        ...obs
                    )

                default:
                    throw new Error(type + ' type does not exist.')
            }
        })


const listReaction = (id, dsId, type, getState) =>
    createList(dsId)
        .mergeMap(list => {
            const entries = list.getEntries()
            return Observable.merge(
                checkEntries(type, entries, getState),
                addEntries(type, entries, getState),
                watchList(type, list, getState)
            )
        })


const checkEntries = (type, ids, getState) => {
    console.log('checkEntries type --->', type, 'ids --->', ids)
    const baseType = collTypeToBasicType(type)

    if (ids.length === 0) {
        return Observable.merge(
            Observable.of(replaceEntries(type, {})),       // if no ids present, remove previous entries.
            Observable.of(changeSelectedEntry(baseType))   // remove selected entry
        )
    }

    const selectedItem = selectSelectedItem(baseType)(getState())
    console.log('selectedItem', selectedItem, {baseType})

    if (Object.keys(selectedItem).length === 0) {
        // selectedItem is empty.
        console.log('selectedItem IS EMPTY--------------')
    }

    const selectedItemId = Object.keys(selectedItem)[0]

    if (ids.indexOf(selectedItemId) > -1) {
        // selectedItemId is available in new items collection.
        return Observable.empty()
    } else {
        // select first item from the selection as the selected entry
        const thisId = ids[0]       // select first id from ids

        // We only want to go down in hierarchy. Upward progression is already in process.
        // Going up might result in inifinite loop.
        return reaction(thisId, baseType, getState, true)
    }
}


const watchList = (type, list, getState) =>
    Observable.merge(
        observeList('entry-added', list),
        observeList('entry-removed', list),
        observeList('entry-moved', list)
    )
    // Just replace with new entries on any list event.
        .mergeMap(() => addEntries(type, list.getEntries(), getState))



// FIXME: TOO HACKY
const addEntries = (type, ids, getState) => {
    console.log('addEntries called')
    const baseType = collTypeToBasicType(type)

    // FIXME: there should be only constants?
    let count = 0

    // We need to replace all the entries in a single shot.
    // Therefore collect all entries objects in "entriesContainer".
    let entriesContainer = {}

    // convert array of entries to individual entries
    return Observable.from(ids)
        .map(id => {
            const dsId = getDsId(id, baseType)
            return {id, dsId}
        })
        .do(x => console.log('addEntries dsId -->', x))
        .mergeMap(({id, dsId}) =>
            observeRecord(dsId)     // Replace with observeRecord() to get record updates forever.
            // getRecordDataOnce(dsId)
                .mergeMap((data) => {
                    console.log('getRecordDataOnce --> data', data)
                    count++
                    if (count <= ids.length) {
                        // for initial values
                        entriesContainer[id] = data

                        // when all entries have been processed.
                        if (count === ids.length) return Observable.of(replaceEntries(type, entriesContainer))
                        // for initial values
                        else return Observable.empty()
                    } else {
                        // count > ids.length
                        // for values coming after replaceEntries
                        console.log('values coming after replaceEntries')

                        return Observable.of(changeEntry(type, id, data))     // dispatch
                    }
                })
        )
}
