/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
// import {addDocriAndSelect, switchDocri} from './thunks'
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap'
import {selectDocries, selectSelectedDocri} from './selectors'
import {ADD_DOCRI} from './constants'
import {addDocriEpic, changeSelectedDocri} from './actionCreators'

const Docries = ({docries, selectedDocri, addDocriAndInitOthers, switchDocri}) => {
    let input
    let selectedDocriId = Object.keys(selectedDocri)[0]

    let docriChange = (e) => {
        let toId = e.target.value
        switchDocri(toId, docries[toId])
    }
    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        addDocriAndInitOthers(input.value)
        input.value = ''
    }
    const displayDocries = () => {
        let docriesOptions = []
        for (let key of Object.keys(docries)) {
            docriesOptions.push(
                <option value={key} key={key}>
                    {docries[key].name}
                </option>
            )
        }
        return docriesOptions
    }
    return (
        <div>
            <FormGroup controlId="formControlsSelect">
                <ControlLabel>Select</ControlLabel>
                <FormControl componentClass="select" placeholder="select" onChange={docriChange}
                             value={selectedDocriId || ''}>
                    {displayDocries()}
                </FormControl>
            </FormGroup>
            <form className="form-inline" onSubmit={submit}>
                <div className="form-group">
                    <label htmlFor="docri-name">Name</label>
                    <input type="text" className="form-control" id="docri-name" ref={node => {
                        input = node
                    }} placeholder="Docri Name"/>
                </div>
                <button type="submit" className="btn btn-default">Create Docri</button>
            </form>
        </div>
    )
}

Docries.propTypes = {
    docries: PropTypes.object.isRequired,
    addDocriAndInitOthers: PropTypes.func.isRequired,
    selectedDocri: PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string
    }),
    switchDocri: PropTypes.func.isRequired
}


const mapDispatchToProps = {
    addDocriAndInitOthers: addDocriEpic,
    switchDocri: changeSelectedDocri
}

const mapStateToProps = createStructuredSelector({
    docries: selectDocries,
    selectedDocri: selectSelectedDocri
})

export default connect(mapStateToProps, mapDispatchToProps)(Docries)
