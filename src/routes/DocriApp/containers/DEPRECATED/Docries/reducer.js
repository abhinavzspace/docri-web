import {fromJS, Map} from 'immutable'

import {
    SELECTED_DOCRI_CHANGE,
    DOCRIES_CHILD_CHANGED_ADDED,
    DOCRIES_CHILD_REMOVED,
    RE_INIT_DOCRIES,
    // CACHE_DOCRIES_LIST,
    DOCRIES_CHILD_ADD_AND_SELECT
} from './constants'


// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    // NO NEED, DOCRIES_CHILD_CHANGED does adding also
    // [DOCRIES_CHILD_ADDED]: (state, action) => {
    //     const docriWithKey = {[action.key]: action.docri}
    //     return state
    //         .mergeIn(['docries'], fromJS(docriWithKey) || Map({}))
    // },

    [DOCRIES_CHILD_CHANGED_ADDED]: (state, action) => {
        return state
            .setIn(['docries', action.key], fromJS(action.docri) || Map({}))
    },

    [DOCRIES_CHILD_REMOVED]: (state, action) => {
        return state
            .deleteIn(['docries', action.key])
    },

    [SELECTED_DOCRI_CHANGE]: (state, action) => {
        const docriWithKey = {[action.key]: action.docri}
        return state
            .set('selectedDocri', fromJS(docriWithKey))
    },

    [RE_INIT_DOCRIES]: (state, action) => {
        return initialState
    },

    // [CACHE_DOCRIES_LIST]: (state, action) => {
    //     return state
    //         .set('listHandle', action.list)
    // }

}

const initialState = fromJS({
    docries: Map({
        // docriId1: {
        //     name: '',
        //     foldersCollectionId: ''
        // },
        // docriId2: {
        //     name: '',
        //     foldersCollectionId: ''
        // }
    }),
    selectedDocri: Map({
        // docriId1: {
        //     name: '',
        //     foldersCollectionId: ''
        // }
    }),
    // listHandle: null
})
function docriesReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default docriesReducer
