/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
// import {addDocriAndSelect, switchDocri} from './thunks'
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap'
import {selectDocries, selectSelectedDocri} from '../selectors'
import {checkChangeSelectedDocri} from '../actionCreators'

const DisplayDocries = ({docries, selectedDocri, switchDocri}) => {
    let selectedDocriId = Object.keys(selectedDocri)[0]

    let docriChange = (e) => {
        let toId = e.target.value
        switchDocri(toId)
    }
    const displayDocries = () => {
        let docriesOptions = []
        for (let key of Object.keys(docries)) {
            docriesOptions.push(
                <option value={key} key={key}>
                    {docries[key].name}
                </option>
            )
        }
        return docriesOptions
    }
    return (
        <div>
            <FormGroup controlId="formControlsSelect">
                <ControlLabel>Select Docri</ControlLabel>
                <FormControl componentClass="select"
                             placeholder="select"
                             onChange={docriChange}
                             value={selectedDocriId || ''}>
                    {displayDocries()}
                </FormControl>
            </FormGroup>
        </div>
    )
}

DisplayDocries.propTypes = {
    docries: PropTypes.object.isRequired,
    selectedDocri: PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string
    }),
    switchDocri: PropTypes.func.isRequired
}


const mapDispatchToProps = {
    switchDocri: checkChangeSelectedDocri
}

const mapStateToProps = createStructuredSelector({
    docries: selectDocries,
    selectedDocri: selectSelectedDocri
})

export default connect(mapStateToProps, mapDispatchToProps)(DisplayDocries)
