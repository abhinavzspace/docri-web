/**
 * Created by abhinav on 10/12/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
// import {addDocriAndSelect, switchDocri} from './thunks'
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap'
import {addDocriEpic} from '../actionCreators'

const CreateDocri = ({addDocriAndInitOthers}) => {
    let input
    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        addDocriAndInitOthers(input.value)
        input.value = ''
    }
    return (
        <div>
            <form className="form-inline" onSubmit={submit}>
                <div className="form-group form-group-sm">
                    <input type="text" className="form-control" id="docri-name" ref={node => {
                        input = node
                    }} placeholder="Docri Name"/>
                </div>
                <button type="submit" className="btn btn-default btn-xs">Create Docri</button>
            </form>
        </div>
    )
}

CreateDocri.propTypes = {
    addDocriAndInitOthers: PropTypes.func.isRequired
}


const mapDispatchToProps = {
    addDocriAndInitOthers: addDocriEpic,
}

const mapStateToProps = createStructuredSelector({})

export default connect(mapStateToProps, mapDispatchToProps)(CreateDocri)
