/**
 * Created by abhinav on 02/12/16.
 */
const debug = require('debug')('app:epics')

import {Observable} from 'rxjs'
import {combineEpics} from 'redux-observable'
import {LISTEN_DOCRIES, ADD_DOCRI, DOCRI_ADDED, DOCRIES_CHILD_CHANGED_ADDED
    , SELECT_FIRST_DOCRI, SELECTED_DOCRI_CHECK} from './constants'
import {observeRecord, observeList, createListObs, checkGetRecord} from '../utils/deepstream'
import {createFoldersCollectionId} from '../Folders/epics'
// import {createFoldersCollection, listenToFolders, selectFirstFolderThunk} from '../Folders/thunks'
// import {listenToNotes, selectFirstNoteThunk} from '../Notes/thunks'
import {changeAddDocri, removeDocri, changeSelectedDocri, justChangeSelectedDocri} from './actionCreators'
import {selectFirstDocri, selectDocriesList} from './selectors'
import {getDs, getUsername} from '../utils/connectors'


// FIXME: NOT THE BEST WAY. Local data should be avoided.
let lastAddedDocriKey

const watchEachEntry = (ds, list) => {
    const entries = list.getEntries()
    // FIXME: there should be only constants?
    let count = 0
    // process entries one by one, dispatch "add" for each entry
    // Once all entries have produced val, dispatch "select first entry"
    return Observable.from(entries)
        .mergeMap(entry =>
            observeRecord(ds, entry)
                .mergeMap(val => {
                    if (count !== null) count++
                    if (count < entries.length || count === null) {
                        // for initial values and values that come over time
                        return Observable.of(changeAddDocri(entry, val))     // dispatch)
                    } else {
                        count = null        // WATCH ONLY INITIAL VALUES
                        return Observable.merge(
                            Observable.of(changeAddDocri(entry, val)),      // dispatch
                            Observable.of({type: SELECT_FIRST_DOCRI})       // dispatch
                        )
                    }

                })
        )
}


export const selectFirstDocriEpic = (action$, {getState}) =>
    action$.ofType(SELECT_FIRST_DOCRI)
        .map(() => selectFirstDocri(getState()))
        .do(({key, docri}) => debug('changeSelectedDocri', changeSelectedDocri(key, docri)))
        .map(({key, docri}) => changeSelectedDocri(key, docri))     // dispatch


const watchAdded = (ds, list) =>
    observeList('entry-added', list)
        .mergeMap(([entry, position]) =>
            observeRecord(ds, entry)
                .map(val => changeAddDocri(entry, val))
        )


// "changeSelectedDocri" if docri is added locally. Don't handle the case for "docri added from remote"
export const checkAddedDocriEpic = (action$, {getState}) =>
    action$.ofType(DOCRIES_CHILD_CHANGED_ADDED)
        .map(action => {
            // action.key is entry
            if (action.key === lastAddedDocriKey) {
                // docri added locally
                lastAddedDocriKey = null
                return changeSelectedDocri(action.key, action.docri)  // NOW, jump to -->
            }
            // docri added from remote
            return {type: 'EPIC_FINISH'}       // FIXME: remove dummy action.
        })


const watchRemoved = (list) =>
    observeList('entry-removed', list)
        .do(([entry, position]) => debug('removeDocri(entry)', removeDocri(entry)))
        .map(([entry, position]) => removeDocri(entry))     // dispatch


const createDocriesList = ({ds, username}) =>
    createListObs(ds, `docriesByUsers/${username}`)


export const watchDocriesEpic = (action$, {getState}) =>
    action$.ofType(LISTEN_DOCRIES)
        .map(() => getState())
        .map(state => ({ds: getDs(), username: getUsername()}))
        .mergeMap(data =>       // FIXME: should be switchMap?
            createDocriesList(data)
                .do(x => debug(x))
                .mergeMap(list => {         // FIXME: should be switchMap?
                    return Observable.merge(
                        // Observable.of(cacheDocriesList(list)),
                        watchEachEntry(data.ds, list),
                        watchAdded(data.ds, list),
                        watchRemoved(list)
                    )
                })
        )


const addDocriEntry = (data) =>
    Observable.create(o => {
        const {action, state} = data
        const ds = getDs()
        const username = getUsername()
        const docri = {
            name: action.name,
            foldersCollectionId: createFoldersCollectionId(ds.getUid(), state)
        }

        const docriId = `${username}/docri/${ds.getUid()}`
        const rec = ds.record.getRecord(docriId)
        const docriesCollId = `docriesByUsers/${username}`
        rec.whenReady(r => {
            r.set(docri)
            o.next({ds, docriesCollId, docriId})
            // const listHandle = selectDocriesList(state)
            // listHandle.addEntry(docriId)
            // o.next({type: DOCRI_ADDED})     // Not really added yet.     // dispatch
        })
        lastAddedDocriKey = docriId
        return () => {
            debug('rec.discard()')
            rec.discard()       // TODO: test this
        }
    })
        .mergeMap(({ds, docriesCollId, docriId}) =>
            createListObs(ds, docriesCollId)
                .map(list => list.addEntry(docriId))
                .map(() => ({type: DOCRI_ADDED}))
        )


// dispatch({type: ADD_DOCRI, name})
export const addDocriEpic = (action$, {getState}) =>
    action$.ofType(ADD_DOCRI)
        .map(action => ({action, state: getState()}))
        .mergeMap(data =>
            addDocriEntry(data)
        )


export const checkChangeSelectedDocriEpic = (action$, {getState}) =>
    action$.ofType(SELECTED_DOCRI_CHECK)
        .mergeMap(action => {
            const ds = getDs()
            const docriId = action.key
            return checkGetRecord(ds, docriId)
                .mergeMap(({alreadyReady, data}) => {
                    if (alreadyReady) {
                        return Observable.of(justChangeSelectedDocri(docriId, data))       // dispatch
                    } else {
                        // TODO: implementation for multi-user remains.
                        throw new Error(docriId + ' does not belong to this user.')
                    }
                })
        })


export default combineEpics(watchDocriesEpic, addDocriEpic, checkAddedDocriEpic
    , selectFirstDocriEpic, checkChangeSelectedDocriEpic)
