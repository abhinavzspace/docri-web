/**
 * Created by abhinav on 07/10/16.
 */
import {createSelector} from 'reselect'
import {getFirstObject} from '../../../../utils'

export const selectDocriState = (state) => state.getIn(['home', 'docries'])

export const selectSelectedDocri = createSelector(
    selectDocriState,
    docriState => docriState.get('selectedDocri').toJS()
)

export const selectDocries = createSelector(
    selectDocriState,
    docriState => docriState.get('docries').toJS()
)

export const selectDocriesList = createSelector(
    selectDocriState,
    docriState => docriState.get('listHandle')
)

export const selectFirstDocri = createSelector(
    selectDocriState,
    docriState => {
        const docries = docriState.get('docries')
        if (!docries) {
            return {
                key: null,
                docri: null
            }
        }

        const firstDocriKey = docries.keySeq().first()
        const firstDocri = docries.valueSeq().first()

        return {
            key: firstDocriKey,
            docri: firstDocri.toJS()
        }
    }
)



// export const selectSelectedDocriId = createSelector(
//     selectSelectedDocri,
//     selectedDocri => selectedDocri.keys().get(0)    // get the only key associated with selectedDocri, see initialState
// )
//
// export const selectSelectedDocriName = createSelector(
//     selectSelectedDocri,
//     selectedDocri => selectedDocri.first().get('name')
// )
//
export const selectFoldersCollectionId = createSelector(
    selectSelectedDocri,
    selectedDocri => {
        const docri = getFirstObject(selectedDocri)
        if (docri) { return docri.foldersCollectionId }
    }
)
