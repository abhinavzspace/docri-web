import {fromJS} from 'immutable'

import {DOCRIES_RECEIVE_DATA} from './constants';
// import {database} from '../../firebaseApp'
import {selectUsername} from '../Auth/selectors'
import {createFoldersCollection, listenToFolders, selectFirstFolderThunk} from '../Folders/thunks'
import {listenToNotes, selectFirstNoteThunk} from '../Notes/thunks'
import {changeAddDocri, removeDocri, changeSelectedDocri} from './actionCreators'
import {selectFirstDocri} from './selectors'

let docriesRefHandle
let lastAddedDocriKey

const getDocriesList = (state) => {
    if (!docriesRefHandle) {
        const username = selectUsername(state)
        docriesRefHandle = getDsClient().record.getList(`docriesByUsers/${username}`)
    }
    return docriesRefHandle
}

let records = {}
const getRecord = (id) => {
    if (!records[id]) {
        records[id] = getDsClient().record.getRecord(id)
    }
    return records[id]
}

export const listenToDocries = () => {
    return (dispatch, getState) => {
        const docriesList = getDocriesList(getState())

        // IMPORTANT: We need 'entry' variable in closure function.
        const subscribeToRecord = (entry) => {
            return new Promise((resolve, reject) => {
                const record = getRecord(entry)

                const recordChangedAdded = (data) => {
                    dispatch(changeAddDocri(entry, data))

                    resolve()

                    if (entry === lastAddedDocriKey) {
                        dispatch(changeSelectedDocri(entry, data))
                        dispatch(listenToFolders(data.foldersCollectionId))
                    }
                    console.log('changeAddDocri', data)
                }

                record.whenReady(r => {
                    r.subscribe(recordChangedAdded, true)
                })

            })
        }

        return new Promise((resolve, reject) => {
            docriesList.whenReady(list => {
                const entries = list.getEntries()
                let subscriptions = []

                for (let entry of entries) {
                    subscriptions.push(subscribeToRecord(entry))
                }

                // Wait for all subscritions first recordChangedAdded to complete.
                Promise.all(subscriptions).then(() => {
                    resolve()
                })

                list.on('entry-added', (entry, position) => {
                    console.log('entry-added', entry, position)
                    subscribeToRecord(entry)

                    // dispatch(addDocri(snapshot.key, snapshot.val()))
                })

                // NOT NEEDED: Implemented already by docriesList.whenReady()
                // docriesList.on('child_changed', (snapshot) => {
                //     dispatch(changeAddDocri(snapshot.key, snapshot.val()))
                // })

                list.on('entry-removed', (entry, position) => {
                    console.log('entry-removed', entry, position)
                    dispatch(removeDocri(entry))
                })

                list.on('error', (message) => {
                    console.log('error', message)
                })
            })


        })
    }
}

export const stopListeningDocries = () => {
    const keysRecords = Object.keys(records)
    for (let key of keysRecords) {
        if (records[key]) {
            records[key].unsubscribe()
            records[key].discard()
            delete records[key]
        }
    }

    if (docriesRefHandle) {
        // NOTE: Lists should not be unsubscribed because they were never subscribed
        docriesRefHandle.discard()      // Directly access docriesRefHandle
        docriesRefHandle = null        // Remove object contained in docriesRefHandle
    }
}

export const addDocriAndSelect = (name) => {
    return (dispatch, getState) => {
        const state = getState()
        const docriesList = getDocriesList(state)

        console.log('adding docri', name)
        const foldersCollectionId = createFoldersCollection(state)
        const docri = {
            name,
            foldersCollectionId
        }

        const username = selectUsername(state)
        const uid = newUid()
        const docriId = `${username}/docri/${uid}`

        getRecord(docriId).whenReady(r => {
            r.set(docri)
            docriesList.addEntry(docriId)
        })

        // const newDocriRef = docriesList.push()
        lastAddedDocriKey = docriId
    }
}

export const switchDocri = (key, docri) => {
    return (dispatch, getState) => {
        dispatch(changeSelectedDocri(key, docri))
        dispatch(listenToFolders()).then(() => {
            dispatch(selectFirstFolderThunk())
        })
    }
}

export const selectFirstDocriThunk = () => {
    return (dispatch, getState) => {
        const {key, docri} = selectFirstDocri(getState())
        if (!key) {
            return
        }
        dispatch(switchDocri(key, docri))
    }
}

// export const startArticleEdit = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT, qid });
//     };
// };
//
// export const cancelArticleEdit = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//     };
// };
//
// export const submitArticleEdit = (qid, content) => {
//     return (dispatch, getState) => {
//         const state = getState();
//         const article = {
//             content,
//             username: state.auth.username,
//             uid: state.auth.uid
//         };
//         dispatch({ type: C.ARTICLE_EDIT_SUBMIT, qid });
//         docriesList.child(qid).set(article, (error) => {
//             dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//             if (error) {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_ERROR,
//                     error: `Update failed! ${error}`
//                 });
//             } else {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_MESSAGE,
//                     message: 'Update successfully saved!'
//                 });
//             }
//         });
//     };
// };
//
// export const deleteArticle = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT_SUBMIT, qid });
//         docriesList.child(qid).remove((error) => {
//             dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//             if (error) {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_ERROR,
//                     error: `Deletion failed! ${error}`
//                 });
//             } else {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_MESSAGE,
//                     message: 'Article successfully deleted!'
//                 });
//             }
//         });
//     };
// };
