/**
 * Created by abhinav on 13/10/16.
 */

import {
    DOCRIES_CHILD_CHANGED_ADDED,
    DOCRIES_CHILD_REMOVED,
    SELECTED_DOCRI_CHANGE,
    RE_INIT_DOCRIES,
    CACHE_DOCRIES_LIST,
    ADD_DOCRI,
    LISTEN_DOCRIES,
    SELECTED_DOCRI_CHECK
} from './constants'

// export const addDocri = (key, docri) => {
//     return {
//         type: DOCRIES_CHILD_ADDED,
//         key,
//         docri
//     }
// }

// export const cacheDocriesList = list => {
//     return {
//         type: CACHE_DOCRIES_LIST,
//         list
//     }
// }

export const changeAddDocri = (key, docri) => {
    return {
        type: DOCRIES_CHILD_CHANGED_ADDED,
        key,
        docri
    }
}

export const removeDocri = (key) => {
    return {
        type: DOCRIES_CHILD_REMOVED,
        key
    }
}

// It may effect change in selected folder and then selected note.
export const changeSelectedDocri = (key, docri) => {
    return {
        type: SELECTED_DOCRI_CHANGE,
        key,
        docri
    }
}

// It will not effect change in selected folder and selected note.
export const justChangeSelectedDocri = (key, docri) => {
    return {
        type: SELECTED_DOCRI_CHANGE,
        key,
        docri
    }
}

export const reInit = () => ({type: RE_INIT_DOCRIES})


// FOR EPICS
export const addDocriEpic = (name) => ({type: ADD_DOCRI, name})
export const listenDocries = () => ({type: LISTEN_DOCRIES})

export const checkChangeSelectedDocri = (key) => {
    return {
        type: SELECTED_DOCRI_CHECK,
        key
    }
}
