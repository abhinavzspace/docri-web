import {fromJS, Map} from 'immutable'

import {
    SELECTED_FOLDER_CHANGE,
    FOLDERS_CHILD_CHANGED_ADDED,
    FOLDERS_CHILD_REMOVED,
    RE_INIT_FOLDERS,
    // CACHE_FOLDERS_HANDLES
} from './constants'

// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    // NO NEED
    // [FOLDERS_CHILD_ADDED]: (state, action) => {
    //     const folderWithKey = {[action.key]: action.folder}
    //     return state
    //         .mergeIn(['foldersCollections', action.collectionId], fromJS(folderWithKey) || Map({}))
    // },

    [FOLDERS_CHILD_CHANGED_ADDED]: (state, action) => {
        return state
            .setIn(['foldersCollections', action.foldersCollectionId, action.key], fromJS(action.folder) || Map({}))
    },

    [FOLDERS_CHILD_REMOVED]: (state, action) => {
        return state
            .deleteIn(['foldersCollections', action.foldersCollectionId, action.key])
    },

    [SELECTED_FOLDER_CHANGE]: (state, action) => {
        const folderWithKey = {[action.key]: action.folder}
        return state
            .set('selectedFolder', fromJS(folderWithKey))
    },

    [RE_INIT_FOLDERS]: (state, action) => {
        return initialState
    },

    // [CACHE_FOLDERS_HANDLES]: (state, action) => {
    //     return state
    //         .setIn(['foldersHandlesByCollectionIds', action.foldersCollectionId], action.handle)
    // }

}

const initialState = fromJS({
    foldersCollections: Map({
        // foldersCollectionId1: {
        //     folderId1: {
        //         name: '',
        //         notesCollectionId: ''
        //         docriId
        //     },
        //     folderId2: {...}
        // },
        // foldersCollectionId2: {
        //     folderId3: {
        //         name: '',
        //         notesCollectionId: ''
        //         docriId
        //     },
        //     folderId4: {...}
        // }
    }),
    selectedFolder: Map({
        // folderId1: {
        //     name: '',
        //     notesCollectionId: ''
        //     docriId
        // }
    }),
    // foldersHandlesByCollectionIds: Map({
    //     // foldersCollectionId1: handle1,
    //     // foldersCollectionId2: handle2
    // })
})
function foldersReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default foldersReducer
