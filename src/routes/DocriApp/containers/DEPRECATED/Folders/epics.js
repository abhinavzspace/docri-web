const debug = require('debug')('app:epics:folders')

import {Observable} from 'rxjs'
import {combineEpics} from 'redux-observable'
import {
    FOLDERS_RECEIVE_DATA, LISTEN_FOLDERS, CACHE_FOLDERS_HANDLES,
    ADD_FOLDER, FOLDER_ADDED, FOLDERS_CHILD_CHANGED_ADDED, SELECT_FIRST_FOLDER
} from './constants';
import {SELECTED_DOCRI_CHANGE} from '../Docries/constants';
// import {database} from '../../firebaseApp'
import {fromJS} from 'immutable'
import {selectFoldersCollectionId, selectSelectedDocri} from '../Docries/selectors'
import {changeSelectedFolder, changeAddFolder, removeFolder} from './actionCreators'
// import {createNotesCollection, listenToNotes, selectFirstNoteThunk, removeSelectedNoteThunk} from '../Notes/thunks'
import {createNotesCollectionId} from '../Notes/epics'
import {removeSelectedNote} from '../Notes/actionCreators'
import {selectFirstFolder, selectFoldersHandles} from './selectors'
import {createListObs, observeList, observeRecord, createListObsReady} from '../utils/deepstream'
import {getDs, getUsername} from '../utils/connectors'

// FIXME: NOT THE BEST WAY. Local data should be avoided.
let lastAddedFolderKey

export const createFoldersCollectionId = (uid, state) => {
    const username = getUsername()
    return `${username}/foldersCollections/${uid}`
}

// FIXME: TOO HACKY
const watchEachEntry = (collectionId, ds, handle) => {
    const entries = handle.getEntries()
    // FIXME: there should be only constants?
    let count = 0
    // convert array of entries to individual entries
    return Observable.from(entries)
        .do(x => debug(x))
        .mergeMap(entry =>
            observeRecord(ds, entry)
                .mergeMap(val => {
                    if (count !== null) count++
                    if (count < entries.length || count === null) {
                        // for initial values and values that come over time
                        debug('count', count)
                        return Observable.of(changeAddFolder(collectionId, entry, val))     // dispatch)
                    } else {
                        count = null        // WATCH ONLY INITIAL VALUES
                        return Observable.merge(
                            Observable.of(changeAddFolder(collectionId, entry, val)),      // dispatch
                            Observable.of({type: SELECT_FIRST_FOLDER})       // dispatch
                        )
                    }
                })
        )
}


export const selectFirstFolderEpic = (action$, {getState}) =>
    action$.ofType(SELECT_FIRST_FOLDER)
        .map(() => selectFirstFolder(getState()))
        .do(({key, folder}) => debug('changeSelectedFolder', changeSelectedFolder(key, folder)))
        .filter(({key}) => key !== null)
        .map(({key, folder}) => changeSelectedFolder(key, folder))     // dispatch


const watchAdded = (collectionId, ds, handle) =>
    observeList('entry-added', handle)
        .mergeMap(([entry, position]) =>
            observeRecord(ds, entry)
                .map(val => changeAddFolder(collectionId, entry, val))
        )


// "changeSelected--" if added locally. Don't handle the case for "added from remote"
export const checkAddedFolderEpic = (action$, {getState}) =>
    action$.ofType(FOLDERS_CHILD_CHANGED_ADDED)
        .map(action => {
            // action.key is entry
            if (action.key === lastAddedFolderKey) {
                // folder added locally
                lastAddedFolderKey = null
                return changeSelectedFolder(action.key, action.folder)  // NOW, jump to -->
            }
            // folder added from remote
            return {type: 'EPIC_FINISH'}       // FIXME: remove dummy action.
        })


const watchRemoved = (collectionId, handle) =>
    observeList('entry-removed', handle)
        .do(([entry, position]) => debug('removeFolder(entry)', removeFolder(collectionId, entry)))
        .map(([entry, position]) => removeFolder(collectionId, entry))     // dispatch


const afterCreateList = (collectionId, ds, handle) =>
    Observable.merge(
        watchEachEntry(collectionId, ds, handle),
        watchAdded(collectionId, ds, handle),
        watchRemoved(collectionId, handle)
    )


const createFoldersList = ({key, docri, ds, username, collectionId}) =>
    createListObs(ds, collectionId)
        .do(x => debug('folders createListObs done', x))
        .mergeMap(handle =>
            afterCreateList(collectionId, ds, handle)
            // Observable.merge(
            //     Observable.of(cacheFoldersHandle(collectionId, handle)),
            //     afterCreateList(collectionId, ds, handle)
            // )
        )


export const observeFoldersEpic = (action$, {getState}) =>
    action$.ofType(SELECTED_DOCRI_CHANGE)
        .map(action => {
            const state = getState()
            return {
                key: action.key,
                docri: action.docri,
                ds: getDs(),
                username: getUsername(),
                collectionId: action.docri.foldersCollectionId,
                // handles: selectFoldersHandles(state)
            }
        })
        .do(x => debug(x))
        .mergeMap(data =>
            createListObsReady(data.ds, data.collectionId)
                .do(x => debug(x))
                .mergeMap(({list, alreadyReady}) => {
                    if (alreadyReady) {
                        // folders were already initialized
                        return Observable.of({type: SELECT_FIRST_FOLDER})       // dispatch
                    } else {
                        return Observable.of(data).mergeMap(data => createFoldersList(data))
                    }
                })
        )
// {
//             const {collectionId} = data
//             if (collectionId in handles) {
//                 // if foldersHandles[foldersCollectionId] is present, it means folders are already initialized
//                 return Observable.of({type: SELECT_FIRST_FOLDER})       // dispatch
//             } else {
//                 return Observable.of(data).mergeMap(data => createFoldersList(data))
//             }
//         })



const addFolderEntry = ({action, state}) =>
    Observable.create(o => {
        const ds = getDs()
        const foldersCollectionId = selectFoldersCollectionId(state)
        const username = getUsername()
        // const handles = selectFoldersHandles(state)
        // const handle = handles[foldersCollectionId]
        const selectedDocri = selectSelectedDocri(state)
        const docriId = Object.keys(selectedDocri)[0]
        const folder = {
            name: action.name,
            notesCollectionId: createNotesCollectionId(ds.getUid(), state),
            docriId
        }
        const folderId = `${username}/folder/${ds.getUid()}`
        const rec = ds.record.getRecord(folderId)
        rec.whenReady(r => {
            r.set(folder)
            o.next({ds, foldersCollectionId, folderId})
            // handle.addEntry(folderId)
            // o.next({type: FOLDER_ADDED})     // dispatch
        })
        lastAddedFolderKey = folderId
        return () => {
            debug('rec.discard()')
            rec.discard()       // TODO: test this
        }
    })
        .mergeMap(({ds, foldersCollectionId, folderId}) =>
            createListObs(ds, foldersCollectionId)
                .map(list => list.addEntry(folderId))
                .map(() => ({type: FOLDER_ADDED}))
        )

// dispatch({type: ADD_FOLDER, name})
export const addFolderEpic = (action$, {getState}) =>
    action$.ofType(ADD_FOLDER)
        .map(action => ({action, state: getState()}))
        .mergeMap(data => addFolderEntry(data))


export default combineEpics(addFolderEpic, checkAddedFolderEpic, observeFoldersEpic, selectFirstFolderEpic)
