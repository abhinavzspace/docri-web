/**
 * Created by abhinav on 13/10/16.
 */

import {
    FOLDERS_CHILD_CHANGED_ADDED,
    FOLDERS_CHILD_REMOVED,
    SELECTED_FOLDER_CHANGE,
    RE_INIT_FOLDERS,
    LISTEN_FOLDERS,
    CACHE_FOLDERS_HANDLES,
    ADD_FOLDER
} from './constants'

// NO NEED
// export const addFolder = (collectionId, key, folder) => {
//     return {
//         type: FOLDERS_CHILD_ADDED,
//         collectionId,
//         key,
//         folder
//     }
// }

// export const cacheFoldersHandles = (foldersCollectionId, handle) => {
//     return {
//         type: CACHE_FOLDERS_HANDLES,
//         foldersCollectionId,
//         handle
//     }
// }

export const listenFolders = (foldersCollectionId, key, folder) => {
    return {
        type: LISTEN_FOLDERS,
        foldersCollectionId,
        key,
        folder
    }
}

export const changeAddFolder = (foldersCollectionId, key, folder) => {
    return {
        type: FOLDERS_CHILD_CHANGED_ADDED,
        foldersCollectionId,
        key,
        folder
    }
}

export const removeFolder = (foldersCollectionId, key) => {
    return {
        type: FOLDERS_CHILD_REMOVED,
        foldersCollectionId,
        key
    }
}

export const changeSelectedFolder = (key, folder) => {
    return {
        type: SELECTED_FOLDER_CHANGE,
        key,
        folder
    }
}

export const reInit = () => {
    return {
        type: RE_INIT_FOLDERS,
    }
}

// FOR EPIC
export const cacheFoldersHandle = (foldersCollectionId, handle) => {
    return {
        type: CACHE_FOLDERS_HANDLES,
        foldersCollectionId,
        handle
    }
}

// FOR EPICS
export const addFolderEpic = (name) => ({type: ADD_FOLDER, name})
