const debug = require('debug')('app:thunks')
import {FOLDERS_RECEIVE_DATA} from './constants';
// import {database} from '../../firebaseApp'
import {fromJS} from 'immutable'
import {selectUsername} from '../Auth/selectors'
import {selectFoldersCollectionId} from '../Docries/selectors'
import {changeSelectedFolder, changeAddFolder, removeFolder} from './actionCreators'
import {createNotesCollection, listenToNotes, selectFirstNoteThunk, removeSelectedNoteThunk} from '../Notes/thunks'
import {selectFirstFolder} from './selectors'

import {getDsClient, newUid} from '../../deepstreamApp'

// const foldersCollectionsRef = database.ref('foldersCollections')
let lastAddedFolderKey
let foldersListsByCollectionIds = {}

const getFoldersList = (foldersCollectionId, username) => {
    let isNewList = false

    // caching
    if (!foldersListsByCollectionIds[foldersCollectionId]) {
        const list = getDsClient().record.getList(`${username}/foldersCollections/${foldersCollectionId}`)
        foldersListsByCollectionIds[foldersCollectionId] = list
        isNewList = true
    }

    return {
        foldersList: foldersListsByCollectionIds[foldersCollectionId],
        isNewList
    }
}

export const createFoldersCollection = (state) => {
    const uid = newUid()
    const username = selectUsername(state)
    getDsClient().record.getList(`${username}/foldersCollections/${uid}`).whenReady(list => {
        foldersListsByCollectionIds[list.name] = list
        return list.name
    })
}

let records = {}
const getRecord = (id) => {
    if (!records[id]) {
        records[id] = getDsClient().record.getRecord(id)
    }
    return records[id]
}


export const listenToFolders = (foldersCollectionId = null) => {
    return (dispatch, getState) => {
        if (foldersCollectionId === null) {
            foldersCollectionId = selectFoldersCollectionId(getState())
        }
        const username = selectUsername(getState())

        // FIXME: destructuring is not working. -> const {foldersList, isNewList} = getFoldersList(...)
        const foldersListObj = getFoldersList(foldersCollectionId, username)
        const foldersList = foldersListObj.foldersList

        if (!foldersListObj.isNewList) {
            return Promise.resolve()
        }

        // foldersList.off()

        const subscribeToRecord = (entry) => {
            return new Promise((resolve, reject) => {
                const record = getRecord(entry)

                const recordChangedAdded = (data) => {
                    debug('folders recordChangedAdded', entry, data)
                    dispatch(changeAddFolder(foldersCollectionId, entry, data))

                    resolve()

                    if (entry === lastAddedFolderKey) {
                        dispatch(changeSelectedFolder(entry, data))
                        dispatch(listenToNotes(data.notesCollectionId))
                    }
                }
                record.whenReady(r => {
                    r.subscribe(recordChangedAdded, true)
                })
            })
        }

        return new Promise((resolve, reject) => {

            foldersList.whenReady(list => {
                const entries = list.getEntries()
                let subscriptions = []

                for (let entry of entries) {
                    subscriptions.push(subscribeToRecord(entry))
                }

                // Wait for all subscritions first recordChangedAdded to complete.
                Promise.all(subscriptions).then(() => {
                    resolve()
                })

                list.on('entry-added', (entry, position) => {
                    console.log('entry-added', entry, position)
                    subscribeToRecord(entry)

                    // dispatch(addDocri(snapshot.key, snapshot.val()))
                })

                // NOT NEEDED: Implemented already by docriesRef.whenReady()
                // docriesRef.on('child_changed', (snapshot) => {
                //     dispatch(changeAddDocri(snapshot.key, snapshot.val()))
                // })

                list.on('entry-removed', (entry, position) => {
                    debug('folders child_removed', entry)
                    dispatch(removeFolder(foldersCollectionId, entry))
                })

                list.on('error', (message) => {
                    debug('error', message)
                })
            })

            /*foldersList.once('value', (snapshot) => {
                // Data will be sent only once over the network by firebase.
                // Therefore no harm in calling this event.
                // We need this event to know when all the data has arrived.
                resolve()
            }, (error) => {
                // dispatch({
                //     type: FOLDERS_RECEIVE_DATA_ERROR,
                //     message: error.message
                // });
                reject()
            })

            foldersList.on('child_added', (snapshot) => {
                debug('folders child_added', foldersCollectionId, snapshot.key, snapshot.val())
                dispatch(addFolder(foldersCollectionId, snapshot.key, snapshot.val()))

                if (snapshot.key === lastAddedFolderKey) {
                    dispatch(changeSelectedFolder(snapshot.key, snapshot.val()))
                }
            })

            foldersList.on('child_changed', (snapshot) => {
                debug('folders child_changed', snapshot.key, snapshot.val())
                dispatch(changeAddFolder(foldersCollectionId, snapshot.key, snapshot.val()))
            })

            foldersList.on('child_removed', (snapshot) => {
                debug('folders child_removed', snapshot.key, snapshot.val())
                dispatch(removeFolder(foldersCollectionId, snapshot.key))
            })*/
        })
    }
}

export const stopListeningFolders = () => {
    const keysRecords = Object.keys(records)
    for (let key of keysRecords) {
        records[key].unsubscribe()
        records[key].discard()
        delete records[key]
    }

    const keysLists = Object.keys(foldersListsByCollectionIds)
    for (let key of keysLists) {
        // NOTE: Lists should not be unsubscribed because they were never subscribed
        foldersListsByCollectionIds[key].discard()
        delete foldersListsByCollectionIds[key]
    }
}

export const addFolderAndSelect = (name) => {
    return (dispatch, getState) => {
        const state = getState()
        const foldersCollectionId = selectFoldersCollectionId(state)
        const username = selectUsername(state)

        // FIXME: destructuring is not working. -> const {foldersList, isNewList} = getFoldersList(...)
        const foldersListObj = getFoldersList(foldersCollectionId, username)
        const foldersList = foldersListObj.foldersList

        const notesCollectionId = createNotesCollection(state)
        const folder = {
            name,
            notesCollectionId
        }

        const uid = newUid()
        const folderId = `${username}/folder/${uid}`

        getRecord(folderId).whenReady(r => {
            r.set(folder)
            foldersList.addEntry(folderId)
        })

        // const newDocriRef = docriesList.push()
        lastAddedFolderKey = folderId

        /*const newFolderRef = foldersList.push()
        lastAddedFolderKey = newFolderRef.key

        // dispatch({ type: C.ARTICLE_AWAIT_CREATION_RESPONSE });
        newFolderRef.set(folder, (error) => {
            // dispatch({ type: C.ARTICLE_RECEIVE_CREATION_RESPONSE });
            if (error) {
                // dispatch({
                //     type: C.FEEDBACK_DISPLAY_ERROR,
                //     error: `Submission failed! ${error}`
                // });
            } else {
                // dispatch({
                //     type: ADD_DOCRI
                // });

                // folder created but notesCollectionId is not yet present in state
                // So provide notesCollectionId
                dispatch(listenToNotes(notesCollectionId))
            }
        })*/
    }
}

export const switchFolder = (key, folder) => {
    return (dispatch, getState) => {
        dispatch(changeSelectedFolder(key, folder))
        dispatch(listenToNotes()).then(() => {
            dispatch(selectFirstNoteThunk())
        })
    }
}

export const selectFirstFolderThunk = () => {
    return (dispatch, getState) => {
        const {key, folder} = selectFirstFolder(getState())
        if (!key) {
            dispatch(removeSelectedNoteThunk())
            return
        }
        dispatch(switchFolder(key, folder))
    }
}

// export const startArticleEdit = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT, qid });
//     };
// };
//
// export const cancelArticleEdit = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//     };
// };
//
// export const submitArticleEdit = (qid, content) => {
//     return (dispatch, getState) => {
//         const state = getState();
//         const article = {
//             content,
//             username: state.auth.username,
//             uid: state.auth.uid
//         };
//         dispatch({ type: C.ARTICLE_EDIT_SUBMIT, qid });
//         foldersList.child(qid).set(article, (error) => {
//             dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//             if (error) {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_ERROR,
//                     error: `Update failed! ${error}`
//                 });
//             } else {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_MESSAGE,
//                     message: 'Update successfully saved!'
//                 });
//             }
//         });
//     };
// };
//
// export const deleteArticle = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT_SUBMIT, qid });
//         foldersList.child(qid).remove((error) => {
//             dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//             if (error) {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_ERROR,
//                     error: `Deletion failed! ${error}`
//                 });
//             } else {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_MESSAGE,
//                     message: 'Article successfully deleted!'
//                 });
//             }
//         });
//     };
// };
