/**
 * Created by abhinav on 07/10/16.
 */
import {createSelector} from 'reselect'
import {getFirstObject} from '../../../../utils'
import {selectFoldersCollectionId} from '../Docries/selectors'

export const selectFoldersState = (state) => state.getIn(['home', 'folders'])

export const selectSelectedFolder = createSelector(
    selectFoldersState,
    foldersState => foldersState.get('selectedFolder').toJS()
)

export const selectFoldersHandles = createSelector(
    selectFoldersState,
    foldersState => foldersState.get('foldersHandlesByCollectionIds').toJS()
)

export const selectFolders = createSelector(
    selectFoldersState,
    selectFoldersCollectionId,
    (foldersState, foldersCollectionId) => {
        const folders = foldersState.getIn(['foldersCollections', foldersCollectionId])
        if (folders) {
            return folders.toJS()
        }
    }
)

export const selectFirstFolder = createSelector(
    selectFoldersState,
    selectFoldersCollectionId,
    (foldersState, foldersCollectionId) => {
        const folders = foldersState.getIn(['foldersCollections', foldersCollectionId])
        if (!folders) {
            return {
                key: null,
                folder: null
            }
        }

        const firstFolderKey = folders.keySeq().first()
        const firstFolder = folders.valueSeq().first()

        return {
            key: firstFolderKey,
            folder: firstFolder.toJS()
        }
    }
)

// export const selectSelectedFolderId = createSelector(
//     selectSelectedFolder,
//     selectedFolder => selectedFolder.keys().get(0)    // get the only key associated with selectedFolder, see initialState
// )
//
// export const selectSelectedFolderName = createSelector(
//     selectSelectedFolder,
//     selectedFolder => selectedFolder.first().get('name')
// )
//
export const selectNotesCollectionId = createSelector(
    selectSelectedFolder,
    selectedFolder => {
        const folder = getFirstObject(selectedFolder)
        if (folder) {
            return folder.notesCollectionId
        }
    }
)
