/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
// import {addFolderAndSelect, switchFolder} from './thunks'
import {addFolderEpic, changeSelectedFolder} from './actionCreators'
import {ListGroupItem, ListGroup} from 'react-bootstrap'
import {selectFolders, selectSelectedFolder} from './selectors'

const Folders = ({folders, selectedFolder, addFolderEpic, changeSelectedFolder}) => {
    let input
    const selectedFolderId = Object.keys(selectedFolder)[0]

    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        addFolderEpic(input.value)
        input.value = ''
    }
    const displayFolders = () => {
        if (!folders) { return }

        let foldersLis = []
        for (let key of Object.keys(folders)) {
            foldersLis.push(
                <ListGroupItem
                    key={key}
                    active={key === selectedFolderId}
                    onClick={() => {
                        if (key !== selectedFolderId) {
                            changeSelectedFolder(key, folders[key])
                        }
                    }}>
                    {folders[key].name}
                </ListGroupItem>
            )
        }
        return foldersLis
    }
    return (
        <div>
            <ListGroup>
                <ListGroupItem key={0}> {'FOLDERS'} </ListGroupItem>
                {displayFolders()}
            </ListGroup>
            <form className="form-inline" onSubmit={submit}>
                <div className="form-group form-group-sm">
                    <input type="text" className="form-control" ref={node => {
                        input = node
                    }} placeholder="Folder Name" style={{width: 100 + '%'}}/>
                </div>
                <button type="submit" className="btn btn-default btn-xs">Create Folder</button>
            </form>
        </div>
    )
}

Folders.propTypes = {
    folders: PropTypes.object,
    addFolderEpic: PropTypes.func.isRequired,
    selectedFolder: PropTypes.object,
    changeSelectedFolder: PropTypes.func.isRequired
}


const mapDispatchToProps = {
    addFolderEpic,
    changeSelectedFolder
}

const mapStateToProps = createStructuredSelector({
    folders: selectFolders,
    selectedFolder: selectSelectedFolder
})

export default connect(mapStateToProps, mapDispatchToProps)(Folders)
