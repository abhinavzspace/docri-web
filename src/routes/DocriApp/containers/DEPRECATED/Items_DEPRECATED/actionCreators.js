/**
 * Created by abhinav on 23/12/16.
 */
import {REPLACE_ENTRIES, CHANGE_ENTRY, CHANGE_SELECTED_ENTRY, RE_INIT, CREATE_ENTRY} from './constants'
import {
    DOCRI_COLL, FOLDER_COLL, NOTE_COLL
    , DOCRI, FOLDER, NOTE, USERNAME
} from '../DEPRECATED/Reaction/constants'

export const getReducerKey = (itemType) => {
    switch (itemType) {
        case NOTE_COLL:
            return 'allNotes'
        case FOLDER_COLL:
            return 'allFolders'
        case DOCRI_COLL:
            return 'allDocries'
        case NOTE:
            return 'selectedNote'
        case FOLDER:
            return 'selectedFolder'
        case DOCRI:
            return 'selectedDocri'
        default:
            throw new Error(itemType, 'This itemType is not possible.')
    }
}


export const createEntryFor = (itemType) => (name) => ({type: CREATE_ENTRY, itemType, name})


// FOR REACTION

export const replaceEntries = (itemType, entriesContainer) => {
    return {
        type: REPLACE_ENTRIES,
        key: getReducerKey(itemType),
        entriesContainer
    }
}


export const changeEntry = (itemType, entry, val) => {
    return {
        type: CHANGE_ENTRY,
        key: getReducerKey(itemType),
        id: entry,
        data: val
    }
}


// If id and data are undefined, removed selectedEntry
export const changeSelectedEntry = (itemType, id, data) => {
    return {
        type: CHANGE_SELECTED_ENTRY,
        key: getReducerKey(itemType),
        id,
        data
    }
}


export const reInit = () => ({type: RE_INIT})
