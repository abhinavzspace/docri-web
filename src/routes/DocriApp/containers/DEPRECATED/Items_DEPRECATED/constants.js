/**
 * Created by abhinav on 23/12/16.
 */

export const REPLACE_ENTRIES = 'REPLACE_ENTRIES'
export const CHANGE_ENTRY = 'CHANGE_ENTRY'
export const CHANGE_SELECTED_ENTRY = 'CHANGE_SELECTED_ENTRY'
export const RE_INIT = 'RE_INIT'
export const CREATE_ENTRY = 'CREATE_ENTRY'
export const ITEM_ROUTE_CHANGE = 'ITEM_ROUTE_CHANGE'
