/**
 * Created by abhinav on 23/12/16.
 */

import {createSelector} from 'reselect'
import {getReducerKey} from './actionCreators'


export const selectItemsState = (state) => state.getIn(['user', 'items'])

export const selectSelectedNote = createSelector(
    selectItemsState,
    (itemsState) => itemsState.get('selectedNote').toJS()
)

// by reducerKey
export const selectSelectedItem = (baseType) => createSelector(
    selectItemsState,
    (itemsState) => {
        const reducerKey = getReducerKey(baseType)
        return itemsState.get(reducerKey).toJS()
    }
)

