/**
 * Created by abhinav on 23/12/16.
 */

import {fromJS, Map} from 'immutable'

import {REPLACE_ENTRIES, CHANGE_ENTRY, CHANGE_SELECTED_ENTRY} from './constants'

// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    [REPLACE_ENTRIES]: (state, action) => {
        return state
            .set(action.key, fromJS(action.entriesContainer))
    },

    [CHANGE_ENTRY]: (state, action) => {
        const itemWithId = {[action.id]: action.data}
        // TODO: test this
        if (!state.hasIn([action.key, action.id]))
            throw new Error(`id: ${action.id} must be present in key: ${action.key}.`)
        return state
            .set(action.key, fromJS(itemWithId))
    },

    [CHANGE_SELECTED_ENTRY]: (state, action) => {
        // re-init if id and data are undefined
        const itemWithId = (action.id && action.data) ? {[action.id]: action.data} : {}
        return state
            .set(action.key, fromJS(itemWithId))
    },

}

const initialState = fromJS({
    allDocries: Map({
        // docriId1: {...},
        // docriId2: {...}
    }),
    selectedDocri: Map({
        // docriId1: {...}
    }),
    allFolders: Map({
        // folderId1: {...},
        // folderId2: {...}
    }),
    selectedFolder: Map({
        // folderId1: {...}
    }),
    allNotes: Map({
        // noteId1: {
        //     name: '',
        //     editorId: ''
        //     folderId
        // },
        // noteId2: {...}
    }),
    selectedNote: Map({
        // noteId1: {
        //     name: '',
        //     editorId: ''
        //     folderId
        // }
    })
})
function itemsReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default itemsReducer
