/**
 * Created by abhinav on 23/12/16.
 */

import {Observable} from 'rxjs'
import {combineEpics} from 'redux-observable'
import {getDs, getUsername} from '../utils/connectors'
import {CREATE_ENTRY, ITEM_ROUTE_CHANGE} from './constants'
import {USERNAME, DOCRI, FOLDER, NOTE, DOCRI_COLL} from '../DEPRECATED/Reaction/constants'
import {selectItemsState} from './selectors'
import {reaction} from '../Reaction'
import {createList} from '../DEPRECATED/deepstream'
import {selectRouteParamsFromState} from '../App/selectors'


const createItem = (data) =>
    Observable.create(o => {
        const {action, state} = data
        const {name, itemType} = action
        const ds = getDs()
        const username = getUsername()
        const itemsState = selectItemsState(state)

        let parentId
        if (itemType === DOCRI)
            parentId = null
        else if (itemType === FOLDER) {
            parentId = itemsState.get('selectedDocri').keySeq().first()
            if (!parentId) throw new Error('No Docri selected.')        // CATCH THIS
        } else if (itemType === NOTE) {
            parentId = itemsState.get('selectedFolder').keySeq().first()
            if (!parentId) throw new Error('No Folder selected.')       // CATCH THIS
        } else throw new Error('This itemType is wrong.', {itemType})

        const dataToSend = {
            itemType,
            name,
            parentId,
            username
        }
        ds.rpc.make('create-item', dataToSend, (err, response) => {
            if (err) { console.error(err) }
            console.log('create-item success response', response)
            o.next({type: 'ENTRY_CREATED_EPIC_FINISH'})         // dispatch
        })


        return () => {}
    })


export const createItemEpic = (action$, {getState}) =>
    action$.ofType(CREATE_ENTRY)
        .map(action => ({action, state: getState()}))
        .mergeMap(data =>
            createItem(data)
        )


export const routeChangeEpic = (action$, {getState}) =>
    action$.ofType(ITEM_ROUTE_CHANGE)   // ds must be present
        .mergeMap(() => {
            const urlParams = selectRouteParamsFromState(getState())
            console.log('routeChangeEpic', {urlParams})
            const {username, item, id} = urlParams.urlParams
            return doReaction(username, item, id, getState)
            // return Observable.empty()
        })


const doReaction = (username, item, id, getState) => {
    const usernameInMemory = getUsername()
    console.log('--doReaction--')

    if (username !== usernameInMemory) {
        console.log('USERNAMES_DIDNT_MATCH')
        return Observable.of({type: 'USERNAMES_DIDNT_MATCH'})       // TODO: show error
    } else if (item === USERNAME) {
        // id not needed
        return reaction(getUsername(), DOCRI_COLL, getState)
    } else if (!(item == DOCRI || item == FOLDER || item == NOTE)) {
        console.log('ITEM_WRONG', {item})
        return Observable.of({type: 'ITEM_WRONG'})                  // TODO: show error
    }
    return reaction(id, item, getState)
}


export default combineEpics(createItemEpic, routeChangeEpic)
