const debug = require('debug')('app:epics:notes')

import {Observable} from 'rxjs'
import {combineEpics} from 'redux-observable'
import {NOTES_RECEIVE_DATA, LISTEN_NOTES, CACHE_NOTES_HANDLES,
    ADD_NOTE, NOTE_ADDED, NOTES_CHILD_CHANGED_ADDED, SELECT_FIRST_NOTE} from './constants';
import {SELECTED_FOLDER_CHANGE} from '../Folders/constants';
import {fromJS} from 'immutable'
import uuid from 'node-uuid'
import {selectNotesCollectionId, selectSelectedFolder} from '../Folders/selectors'
import {changeSelectedNote, changeAddNote, removeNote} from './actionCreators'
// import {createNotesCollection, listenToNotes, selectFirstNoteThunk, removeSelectedNoteThunk} from '../Notes/thunks'
import {selectFirstNote, selectNotesHandles} from './selectors'
import {createListObs, observeList, observeRecord, createListObsReady} from '../utils/deepstream'
import {getDs, getUsername} from '../utils/connectors'

// FIXME: NOT THE BEST WAY. Local data should be avoided.
let lastAddedNoteKey

export const createNotesCollectionId = (uid, state) => {
    const username = getUsername()
    return `${username}/notesCollections/${uid}`
}


// FIXME: TOO HACKY
const watchEachEntry = (collectionId, ds, handle) => {
    const entries = handle.getEntries()
    // FIXME: there should be only constants?
    let count = 0
    // convert array of entries to individual entries
    return Observable.from(entries)
        .do(x => debug(x))
        .mergeMap(entry =>
            observeRecord(ds, entry)
                .mergeMap(val => {
                    if (count !== null) count++
                    if (count < entries.length || count === null) {
                        // for initial values and values that come over time
                        return Observable.of(changeAddNote(collectionId, entry, val))     // dispatch)
                    } else {
                        count = null        // WATCH ONLY INITIAL VALUES
                        return Observable.merge(
                            Observable.of(changeAddNote(collectionId, entry, val)),      // dispatch
                            Observable.of({type: SELECT_FIRST_NOTE})       // dispatch
                        )
                    }
                })
        )
}

// const watchEachEntry = (collectionId, ds, handle) =>
//     // convert array of entries to individual entries
//     Observable.from(handle.getEntries())
//         .do(x => console.log(x))
//         .mergeMap(entry =>
//             observeRecord(ds, entry)
//                 .do(val => console.log('changeAddNote', changeAddNote(collectionId, entry, val)))
//                 .map(val => changeAddNote(collectionId, entry, val))    // dispatch
//         )


export const selectFirstNoteEpic = (action$, {getState}) =>
    action$.ofType(SELECT_FIRST_NOTE)
        .map(() => selectFirstNote(getState()))
        .do(({key, note}) => debug('changeSelectedNote', changeSelectedNote(key, note)))
        .filter(({key}) => key !== null)
        .map(({key, note}) => changeSelectedNote(key, note))     // dispatch


const watchAdded = (collectionId, ds, handle) =>
    observeList('entry-added', handle)
        .mergeMap(([entry, position]) =>
            observeRecord(ds, entry)
                .map(val => changeAddNote(collectionId, entry, val))
        )


// "changeSelected--" if added locally. Don't handle the case for "added from remote"
export const checkAddedNoteEpic = (action$, {getState}) =>
    action$.ofType(NOTES_CHILD_CHANGED_ADDED)
        .map(action => {
            // action.key is entry
            if (action.key === lastAddedNoteKey) {
                // note added locally
                lastAddedNoteKey = null
                return changeSelectedNote(action.key, action.note)  // NOW, jump to -->
            }
            // note added from remote
            return {type: 'EPIC_FINISH'}       // FIXME: remove dummy action.
        })


const watchRemoved = (collectionId, handle) =>
    observeList('entry-removed', handle)
        .do(([entry, position]) => console.log('removeNote', removeNote(collectionId, entry)))
        .map(([entry, position]) => removeNote(collectionId, entry))     // dispatch


const afterCreateList = (collectionId, ds, handle) =>
    Observable.merge(
        watchEachEntry(collectionId, ds, handle),
        watchAdded(collectionId, ds, handle),
        watchRemoved(collectionId, handle)
    )


const createNotesList = ({key, folder, ds, username, collectionId}) =>
    createListObs(ds, collectionId)
        .do(x => console.log(x))
        .mergeMap(handle =>
                afterCreateList(collectionId, ds, handle)
            // Observable.merge(
            //     Observable.of(cacheNotesHandle(collectionId, handle)),
            //     afterCreateList(collectionId, ds, handle)
            // )
        )


export const observeNotesEpic = (action$, {getState}) =>
    action$.ofType(SELECTED_FOLDER_CHANGE)
        .map(action => {
            const state = getState()
            return {
                key: action.key,
                folder: action.folder,
                ds: getDs(),
                username: getUsername(),
                collectionId: action.folder.notesCollectionId,
                // handles: selectNotesHandles(state)
            }
        })
        .mergeMap(data =>
            createListObsReady(data.ds, data.collectionId)
                .do(x => debug(x))
                .mergeMap(({list, alreadyReady}) => {
                    if (alreadyReady) {
                        // folders were already initialized
                        return Observable.of({type: SELECT_FIRST_NOTE})       // dispatch
                    } else {
                        return Observable.of(data).mergeMap(data => createNotesList(data))
                    }
                })
        )
        // .mergeMap(data => {
        //     const {collectionId, handles} = data
        //     if (collectionId in handles) {
        //         // if notesHandles[notesCollectionId] is present, it means notes are already initialized
        //         return Observable.of({type: SELECT_FIRST_NOTE})       // dispatch
        //     } else {
        //         return Observable.of(data).mergeMap(data => createNotesList(data))
        //     }
        // })


const addNoteEntry = ({action, state}) =>
    Observable.create(o => {
        const ds = getDs()
        const notesCollectionId = selectNotesCollectionId(state)
        // const handles = selectNotesHandles(state)
        // const handle = handles[notesCollectionId]
        const selectedFolder = selectSelectedFolder(state)
        const folderId = Object.keys(selectedFolder)[0]
        const note = {
            name: action.name,
            editorId: uuid.v4(),
            folderId
        }
        const noteId = `${getUsername()}/note/${ds.getUid()}`
        const rec = ds.record.getRecord(noteId)
        rec.whenReady(r => {
            r.set(note)
            o.next({ds, notesCollectionId, noteId})
            // handle.addEntry(noteId)
            // o.next({type: NOTE_ADDED})     // dispatch
        })
        lastAddedNoteKey = noteId
        return () => {
            console.log('rec.discard()')
            rec.discard()       // TODO: test this
        }
    })
        .mergeMap(({ds, notesCollectionId, noteId}) =>
            createListObs(ds, notesCollectionId)
                .map(list => list.addEntry(noteId))
                .map(() => ({type: NOTE_ADDED}))
        )

// dispatch({type: ADD_NOTE, name})
export const addNoteEpic = (action$, {getState}) =>
    action$.ofType(ADD_NOTE)
        .map(action => ({action, state: getState()}))
        .mergeMap(data =>
            addNoteEntry(data)
        )


export default combineEpics(addNoteEpic, checkAddedNoteEpic, observeNotesEpic, selectFirstNoteEpic)
