/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
// import {addNoteAndSelect, switchNote} from './thunks'
import {ListGroupItem, ListGroup} from 'react-bootstrap'
import {selectNotes, selectSelectedNote} from './selectors'
import {addNoteEpic, changeSelectedNote} from './actionCreators'

const Notes = ({notes, selectedNote, addNoteEpic, changeSelectedNote}) => {
    let input
    const selectedNoteId = Object.keys(selectedNote)[0]

    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        addNoteEpic(input.value)
        input.value = ''
    }
    const displayNotes = () => {
        if (!notes) { return }

        let notesLis = []
        for (let key of Object.keys(notes)) {
            notesLis.push(
                <ListGroupItem
                    key={key}
                    active={key === selectedNoteId}
                    onClick={() => {
                        if (key !== selectedNoteId) {
                            changeSelectedNote(key, notes[key])
                        }
                    }}>
                    {notes[key].name}
                </ListGroupItem>
            )
        }
        return notesLis
    }
    return (
        <div style={{paddingLeft: 2 + 'px'}}>
            <ListGroup>
                <ListGroupItem key={0}> {'NOTES'} </ListGroupItem>
                {displayNotes()}
            </ListGroup>
            <form className="form-inline" onSubmit={submit}>
                <div className="form-group form-group-sm">
                    <input type="text" className="form-control" ref={node => {
                        input = node
                    }} placeholder="Note Name" style={{width: 100 + '%'}}/>
                </div>
                <button type="submit" className="btn btn-default btn-xs">Create Note</button>
            </form>
        </div>
    )
}

Notes.propTypes = {
    notes: PropTypes.object,
    addNoteEpic: PropTypes.func.isRequired,
    selectedNote: PropTypes.object,
    changeSelectedNote: PropTypes.func.isRequired
}


const mapDispatchToProps = {
    addNoteEpic,
    changeSelectedNote
}

const mapStateToProps = createStructuredSelector({
    notes: selectNotes,
    selectedNote: selectSelectedNote
})

export default connect(mapStateToProps, mapDispatchToProps)(Notes)
