import debugLib from 'debug'
const debug = debugLib('app:thunks')
import {FOLDERS_RECEIVE_DATA} from './constants';
// import {database} from '../../firebaseApp'
import {fromJS} from 'immutable'
import uuid from 'node-uuid'
import {selectUsername} from '../Auth/selectors'
import {selectNotesCollectionId} from '../Folders/selectors'
import {changeAddNote, removeNote, changeSelectedNote} from './actionCreators'
import {selectFirstNote} from './selectors'

import {getDsClient, newUid} from '../../deepstreamApp'

// const notesCollectionsRef = database.ref('notesCollections')
let lastAddedNoteKey
let notesListsByCollectionIds = {}

const getNotesList = (notesCollectionId, username) => {
    let isNewList = false

    // caching
    if (!notesListsByCollectionIds[notesCollectionId]) {
        const list = getDsClient().record.getList(`${username}/notesCollections/${notesCollectionId}`)
        notesListsByCollectionIds[notesCollectionId] = list
        isNewList = true
    }

    return {
        notesList: notesListsByCollectionIds[notesCollectionId],
        isNewList
    }
}

export const createNotesCollection = (state) => {
    const uid = newUid()
    const username = selectUsername(state)
    const list = getDsClient().record.getList(`${username}/notesCollections/${uid}`)
    notesListsByCollectionIds[list.name] = list
    return list.name
}

let records = {}        // records cache
const getRecord = (id) => {
    if (!records[id]) {
        records[id] = getDsClient().record.getRecord(id)
    }
    return records[id]
}

export const listenToNotes = (notesCollectionId = null) => {
    return (dispatch, getState) => {
        if (notesCollectionId === null) {
            notesCollectionId = selectNotesCollectionId(getState())
        }
        const username = selectUsername(getState())

        // FIXME: destructuring is not working. -> const {notesList, isNewList} = getNotesList(...)
        const notesListObj = getNotesList(notesCollectionId, username)
        const notesList = notesListObj.notesList

        if (!notesListObj.isNewList) {
            return Promise.resolve()
        }

        // notesList.off()

        const subscribeToRecord = (entry) => {
            return new Promise((resolve, reject) => {
                const record = getRecord(entry)

                const recordChangedAdded = (data) => {
                    debug('notes child_changed', entry, data)
                    dispatch(changeAddNote(notesCollectionId, entry, data))

                    resolve()

                    if (entry === lastAddedNoteKey) {
                        dispatch(changeSelectedNote(entry, data))
                    }
                }
                record.whenReady(r => {
                    r.subscribe(recordChangedAdded, true)
                })
            })
        }

        return new Promise((resolve, reject) => {

            notesList.whenReady(list => {
                const entries = list.getEntries()
                let subscriptions = []

                for (let entry of entries) {
                    subscriptions.push(subscribeToRecord(entry))
                }

                // Wait for all subscritions first recordChangedAdded to complete.
                Promise.all(subscriptions).then(() => {
                    resolve()
                })

                list.on('entry-added', (entry, position) => {
                    console.log('entry-added', entry, position)
                    subscribeToRecord(entry)

                    // dispatch(addDocri(snapshot.key, snapshot.val()))
                })

                // NOT NEEDED: Implemented already by docriesRef.whenReady()
                // docriesRef.on('child_changed', (snapshot) => {
                //     dispatch(changeAddDocri(snapshot.key, snapshot.val()))
                // })

                list.on('entry-removed', (entry, position) => {
                    debug('notes child_removed', entry)
                    dispatch(removeNote(notesCollectionId, entry))
                })

                list.on('error', (message) => {
                    debug('error', message)
                })
            })

            /*notesList.on('value', (snapshot) => {
                resolve()
            }, (error) => {
                // dispatch({
                //     type: FOLDERS_RECEIVE_DATA_ERROR,
                //     message: error.message
                // });
                reject()
            })

            notesList.on('child_added', (snapshot) => {
                debug('notes child_added', notesCollectionId, snapshot.key, snapshot.val())
                dispatch(addNote(notesCollectionId, snapshot.key, snapshot.val()))

                if (snapshot.key === lastAddedNoteKey) {
                    dispatch(switchNote(snapshot.key, snapshot.val()))
                }
            })

            notesList.on('child_changed', (snapshot) => {
                debug('notes child_changed', snapshot.key, snapshot.val())
                dispatch(changeNote(notesCollectionId, snapshot.key, snapshot.val()))
            })

            notesList.on('child_removed', (snapshot) => {
                debug('notes child_removed', snapshot.key, snapshot.val())
                dispatch(removeNote(notesCollectionId, snapshot.key))
            })*/
        })
    }
}

export const stopListeningNotes = () => {
    const keysRecords = Object.keys(records)
    for (let key of keysRecords) {
        records[key].unsubscribe()
        records[key].discard()
        delete records[key]
    }

    const keysLists = Object.keys(notesListsByCollectionIds)
    for (let key of keysLists) {
        // NOTE: Lists should not be unsubscribed because they were never subscribed
        notesListsByCollectionIds[key].discard()
        delete notesListsByCollectionIds[key]
    }
}

export const addNoteAndSelect = (name) => {
    return (dispatch, getState) => {
        const state = getState()
        const notesCollectionId = selectNotesCollectionId(state)
        const username = selectUsername(state)

        // FIXME: destructuring is not working. -> const {notesList, isNewList} = getNotesList(...)
        const notesListObj = getNotesList(notesCollectionId, username)
        const notesList = notesListObj.notesList

        const note = {
            name,
            editorId: uuid.v4()
        }

        const uid = newUid()
        const noteId = `${username}/note/${uid}`

        getRecord(noteId).whenReady(r => {
            r.set(note)
            notesList.addEntry(noteId)
        })

        // const newDocriRef = docriesList.push()
        lastAddedNoteKey = noteId

        // // dispatch({ type: C.ARTICLE_AWAIT_CREATION_RESPONSE });
        // newNoteRef.set(note, (error) => {
        //     // dispatch({ type: C.ARTICLE_RECEIVE_CREATION_RESPONSE });
        //     if (error) {
        //         // dispatch({
        //         //     type: C.FEEDBACK_DISPLAY_ERROR,
        //         //     error: `Submission failed! ${error}`
        //         // });
        //     } else {
        //         // dispatch({
        //         //     type: ADD_DOCRI
        //         // });
        //     }
        // })
    }
}

export const switchNote = (key, note) => {
    return (dispatch, getState) => {
        dispatch(changeSelectedNote(key, note))
        // TODO: change editor content
    }
}

export const selectFirstNoteThunk = () => {
    return (dispatch, getState) => {
        const {key, note} = selectFirstNote(getState())
        dispatch(switchNote(key, note))
    }
}

export const removeSelectedNoteThunk = () => {
    return (dispatch, getState) => {
        dispatch(changeSelectedNote(null, null))
    }
}

// export const startArticleEdit = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT, qid });
//     };
// };
//
// export const cancelArticleEdit = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//     };
// };
//
// export const submitArticleEdit = (qid, content) => {
//     return (dispatch, getState) => {
//         const state = getState();
//         const article = {
//             content,
//             username: state.auth.username,
//             uid: state.auth.uid
//         };
//         dispatch({ type: C.ARTICLE_EDIT_SUBMIT, qid });
//         notesList.child(qid).set(article, (error) => {
//             dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//             if (error) {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_ERROR,
//                     error: `Update failed! ${error}`
//                 });
//             } else {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_MESSAGE,
//                     message: 'Update successfully saved!'
//                 });
//             }
//         });
//     };
// };
//
// export const deleteArticle = (qid) => {
//     return (dispatch) => {
//         dispatch({ type: C.ARTICLE_EDIT_SUBMIT, qid });
//         notesList.child(qid).remove((error) => {
//             dispatch({ type: C.ARTICLE_EDIT_FINISH, qid });
//             if (error) {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_ERROR,
//                     error: `Deletion failed! ${error}`
//                 });
//             } else {
//                 dispatch({
//                     type: C.FEEDBACK_DISPLAY_MESSAGE,
//                     message: 'Article successfully deleted!'
//                 });
//             }
//         });
//     };
// };
