/**
 * Created by abhinav on 13/10/16.
 */

import {
    NOTES_CHILD_CHANGED_ADDED,
    NOTES_CHILD_REMOVED,
    SELECTED_NOTE_CHANGE,
    RE_INIT_NOTES,
    // CACHE_NOTES_HANDLES,
    ADD_NOTE,
    SELECTED_NOTE_REMOVE
} from './constants'

// NOT NEEDED
// export const addNote = (collectionId, key, note) => {
//     return {
//         type: NOTES_CHILD_ADDED,
//         collectionId,
//         key,
//         note
//     }
// }

export const changeAddNote = (collectionId, key, note) => {
    return {
        type: NOTES_CHILD_CHANGED_ADDED,
        collectionId,
        key,
        note
    }
}

export const removeNote = (collectionId, key) => {
    return {
        type: NOTES_CHILD_REMOVED,
        collectionId,
        key
    }
}

export const changeSelectedNote = (key, note) => {
    return {
        type: SELECTED_NOTE_CHANGE,
        key,
        note
    }
}

export const removeSelectedNote = () => {
    return {
        type: SELECTED_NOTE_REMOVE
    }
}

export const reInit = () => {
    return {
        type: RE_INIT_NOTES,
    }
}


// FOR EPIC
// export const cacheNotesHandle = (notesCollectionId, handle) => {
//     return {
//         type: CACHE_NOTES_HANDLES,
//         notesCollectionId,
//         handle
//     }
// }

// FOR EPICS
export const addNoteEpic = (name) => ({type: ADD_NOTE, name})
