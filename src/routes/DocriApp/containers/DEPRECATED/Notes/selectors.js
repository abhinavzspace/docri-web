/**
 * Created by abhinav on 07/10/16.
 */
import {createSelector} from 'reselect'
import {getFirstObject} from '../../../../utils'
import {selectNotesCollectionId} from '../Folders/selectors'

export const selectNotesState = (state) => state.getIn(['home', 'notes'])

export const selectSelectedNote = createSelector(
    selectNotesState,
    notesState => notesState.get('selectedNote').toJS()
)

export const isNoteSelected = createSelector(
    selectSelectedNote,
    // if length of keys equals zero, no note is selected
    selectedNote => Object.keys(selectedNote).length == 0 ? false : true
)

export const selectNotesHandles = createSelector(
    selectNotesState,
    notesState => notesState.get('notesHandlesByCollectionIds').toJS()
)

export const selectNotes = createSelector(
    selectNotesState,
    selectNotesCollectionId,
    (notesState, notesCollectionId) => {
        const notes = notesState.getIn(['notesCollections', notesCollectionId])
        if (notes) {
            return notes.toJS()
        }
    }
)

export const selectFirstNote = createSelector(
    selectNotesState,
    selectNotesCollectionId,
    (notesState, notesCollectionId) => {
        const notes = notesState.getIn(['notesCollections', notesCollectionId])
        if (!notes) {
            return {
                key: null,
                note: null
            }
        }

        const firstNoteKey = notes.keySeq().first()
        const firstNote = notes.valueSeq().first()

        return {
            key: firstNoteKey,
            note: firstNote
        }
    }
)
