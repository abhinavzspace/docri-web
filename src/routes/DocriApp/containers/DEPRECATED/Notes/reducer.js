import {fromJS, Map} from 'immutable'

import {
    NOTES_CHILD_CHANGED_ADDED,
    NOTES_CHILD_REMOVED,
    SELECTED_NOTE_CHANGE,
    RE_INIT_NOTES,
    // CACHE_NOTES_HANDLES,
    SELECTED_NOTE_REMOVE
} from './constants'
import {selectNotesCollectionId} from '../Folders/selectors'

// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    // NOT NEEDED
    // [NOTES_CHILD_ADDED]: (state, action) => {
    //     const noteWithKey = {[action.key]: action.note}
    //     return state
    //         .mergeIn(['notesCollections', action.collectionId], fromJS(noteWithKey) || Map({}))
    // },

    [NOTES_CHILD_CHANGED_ADDED]: (state, action) => {
        return state
            .setIn(['notesCollections', action.collectionId, action.key], fromJS(action.note) || Map({}))
    },

    [NOTES_CHILD_REMOVED]: (state, action) => {
        return state
            .deleteIn(['notesCollections', action.collectionId, action.key])
    },

    [SELECTED_NOTE_CHANGE]: (state, action) => {
        const noteWithKey = {[action.key]: action.note}
        return state
            .set('selectedNote', fromJS(noteWithKey))
    },

    [SELECTED_NOTE_REMOVE]: (state, action) => {
        const noteWithKey = {}
        return state
            .set('selectedNote', fromJS(noteWithKey))
    },

    [RE_INIT_NOTES]: (state, action) => {
        return initialState
    },

    // [CACHE_NOTES_HANDLES]: (state, action) => {
    //     return state
    //         .setIn(['notesHandlesByCollectionIds', action.notesCollectionId], action.handle)
    // }

}

const initialState = fromJS({
    notesCollections: Map({
        // notesCollectionId1: {
        //     noteId1: {
        //         name: '',
        //     },
        //     noteId2: {...}
        // },
        // notesCollectionId2: {
        //     noteId3: {
        //         name: '',
        //     },
        //     noteId4: {...}
        // }
    }),
    selectedNote: Map({
        // noteId1: {
        //     name: '',
        //     editorId: ''
        //     folderId
        // }
    }),
    // notesHandlesByCollectionIds: Map({
    //     // notesCollectionId1: handle1,
    //     // notesCollectionId2: handle2
    // })
})
function notesReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default notesReducer
