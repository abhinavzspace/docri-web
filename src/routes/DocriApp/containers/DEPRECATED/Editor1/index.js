/**
 * Created by abhinav on 18/10/16.
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import IO from 'socket.io-client'

import './menu.css'
import './example-setup.css'

import {EditorConnection} from './client/collab'
const {Reporter} = require("./client/reporter")

// const socket = IO('http://localhost:5555')

let connection = null
class Editor extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <div id="editor" ref={(c) => this._editor1 = c}></div>
                <div ref={(c) => this._editor2 = c}></div>
                <div style={{display: "none"}} ref={(c) => this._editorContent = c}>
                    <h1>Using ProseMirror</h1>

                    <p>This is editable text. Focus it and start typing.</p>
                </div>
            </div>
        )
    }

    componentWillMount() {

    }

    componentDidMount() {
        const report = new Reporter()
        // if (connection) connection.close()
        connection = window.connection = new EditorConnection(report, "/docs/" + 'Example')
        // connection.request.then(() => connection.view.editor.focus())

        // let content = this._editorContent
        // content.style.display = "none"
        //
        // let doc = DOMParser.fromSchema(this.schema).parse(content)
        // const authority = new Authority(doc)
        //
        //
        // let editorView1 = collabEditor(authority, this._editor1)
        // let editorView2 = collabEditor(authority, this._editor2)

    }
}

export default Editor
