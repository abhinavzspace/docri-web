/**
 * Created by abhinav on 18/10/16.
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import IO from 'socket.io-client'

const {EditorState} = require("prosemirror-state")
const {EditorView} = require('prosemirror-view')
const {MenuBarEditorView} = require("prosemirror-menu")
const {DOMParser, Schema} = require("prosemirror-model")
const {schema: baseSchema} = require("prosemirror-schema-basic")
const {addListNodes} = require("prosemirror-schema-list")
const {exampleSetup} = require("prosemirror-example-setup")
const collab = require("prosemirror-collab")

import './menu.css'
import './example-setup.css'

// const socket = IO('http://localhost:5555')

class Editor extends React.Component {
    constructor(props) {
        super(props)
        this.schema = new Schema({
            nodes: addListNodes(baseSchema.nodeSpec, "paragraph block*", "block"),
            marks: baseSchema.markSpec
        })
    }

    render() {
        return (
            <div>
                <div ref={(c) => this._editor1 = c}></div>
                <div ref={(c) => this._editor2 = c}></div>
                <div style={{display: "none"}} ref={(c) => this._editorContent = c}>
                    <h1>Using ProseMirror</h1>

                    <p>This is editable text. Focus it and start typing.</p>
                </div>
            </div>
        )
    }

    componentWillMount() {

    }

    componentDidMount() {
        let content = this._editorContent
        content.style.display = "none"

        // let tip = document.querySelector(".demotip")
        //
        // let view = new MenuBarEditorView(this._editor, {
        //     state: EditorState.create({
        //         doc: DOMParser.fromSchema(this.schema).parse(content),
        //         plugins: [exampleSetup({schema: this.schema})]
        //     }),
        //     onAction(action) {
        //         view.updateState(view.editor.state.applyAction(action))
        //     },
        //     onFocus() {
        //         if (tip) {
        //             tip.innerHTML = "<a href='#demos' style='text-decoration: none; pointer-events: auto; color: inherit'>Find more demos below ↓</a>"
        //             tip = null
        //         }
        //     }
        // })


        // function Authority(doc) {
        //     this.doc = doc
        //     this.steps = []
        //     this.stepClientIDs = []
        //     this.onNewSteps = []
        // }
        //
        // Authority.prototype.receiveSteps = function(version, steps, clientID) {
        //     if (version != this.steps.length) return
        //
        //     let self = this
        //     // Apply and accumulate new steps
        //     steps.forEach(function(step) {
        //         self.doc = step.apply(self.doc).doc
        //         self.steps.push(step)
        //         self.stepClientIDs.push(clientID)
        //     })
        //     // Signal listeners
        //     this.onNewSteps.forEach(function(f) { f() })
        // }
        //
        // Authority.prototype.stepsSince = function(version) {
        //     return {
        //         steps: this.steps.slice(version),
        //         clientIDs: this.stepClientIDs.slice(version)
        //     }
        // }




        // const collabEditor = (socketAuthority, place) => {
        //     let view = new EditorView(place, {
        //         state: EditorState.create({
        //             doc: DOMParser.fromSchema(this.schema).parse(content),
        //             plugins: [collab.collab(), exampleSetup({schema: this.schema})]
        //         }),
        //         onAction: function(action) {
        //             let newState = view.state.applyAction(action)
        //             view.updateState(newState)
        //             let sendable = collab.sendableSteps(newState)
        //             if (sendable)
        //                 socketAuthority.emit('receiveSteps', sendable.version, sendable.steps, sendable.clientID)
        //         }
        //     })
        //
        //     socketAuthority.on('newStepsArrive', (stepsSince) => {
        //         let newData = stepsSince(collab.getVersion(view.state))
        //         view.props.onAction(
        //             collab.receiveAction(view.state, newData.steps, newData.clientIDs))
        //     })
        //
        //     return view
        // }
        //
        // let doc = DOMParser.fromSchema(this.schema).parse(content)
        // socket.emit('receiveDoc', doc)
        //
        // let editorView1 = collabEditor(socket, this._editor1)
        // let editorView2 = collabEditor(socket, this._editor2)



        function Authority(doc) {
            this.doc = doc
            this.steps = []
            this.stepClientIDs = []
            this.onNewSteps = []
        }

        Authority.prototype.receiveSteps = function(version, steps, clientID) {
            if (version != this.steps.length) return

            let self = this
            // Apply and accumulate new steps
            steps.forEach(function(step) {
                self.doc = step.apply(self.doc).doc
                self.steps.push(step)
                self.stepClientIDs.push(clientID)
            })
            // Signal listeners
            this.onNewSteps.forEach(function(f) { f() })
        }

        Authority.prototype.stepsSince = function(version) {
            return {
                steps: this.steps.slice(version),
                clientIDs: this.stepClientIDs.slice(version)
            }
        }

        const collabEditor = (authority, place) => {
            let view = new EditorView(place, {
                state: EditorState.create({
                    doc: DOMParser.fromSchema(this.schema).parse(content),
                    plugins: [collab.collab(), exampleSetup({schema: this.schema})]
                }),
                onAction: function(action) {
                    let newState = view.state.applyAction(action)
                    view.updateState(newState)
                    let sendable = collab.sendableSteps(newState)
                    if (sendable)
                        authority.receiveSteps(sendable.version, sendable.steps,
                            sendable.clientID)
                }
            })

            authority.onNewSteps.push(function() {
                let newData = authority.stepsSince(collab.getVersion(view.state))
                view.props.onAction(
                    collab.receiveAction(view.state, newData.steps, newData.clientIDs))
            })

            return view
        }

        let doc = DOMParser.fromSchema(this.schema).parse(content)
        const authority = new Authority(doc)


        let editorView1 = collabEditor(authority, this._editor1)
        let editorView2 = collabEditor(authority, this._editor2)

    }
}





function AuthorityX(doc) {
    this.doc = doc
    this.steps = []
    this.stepClientIDs = []
    this.onNewSteps = []
}

AuthorityX.prototype.receiveSteps = function(version, steps, clientID) {
    if (version != this.steps.length) return

    var self = this
    // Apply and accumulate new steps
    steps.forEach(function(step) {
        self.doc = step.apply(self.doc).doc
        self.steps.push(step)
        self.stepClientIDs.push(clientID)
    })
    // Signal listeners
    this.onNewSteps.forEach(function(f) { f() })
}

AuthorityX.prototype.stepsSince = function(version) {
    return {
        steps: this.steps.slice(version),
        clientIDs: this.stepClientIDs.slice(version)
    }
}



function collabEditorX(authority, place) {
    var view = new EditorView(place, {
        state: EditorState.create({schema: schema, plugins: [collab.collab]}),
        onAction: function(action) {
            var newState = view.state.applyAction(action)
            view.updateState(newState)
            var sendable = collab.sendableSteps(newState)
            if (sendable)
                authority.receiveSteps(sendable.version, sendable.steps,
                    sendable.clientID)
        }
    })

    authority.onNewSteps.push(function() {
        var newData = authority.stepsSince(collab.getVersion(view.state))
        view.props.onAction(
            collab.receiveAction(view.state, newData.steps, newData.clientIDs))
    })

    return view
}

export default Editor
