/**
 * Created by abhinav on 18/10/16.
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {createStructuredSelector} from 'reselect'
import IO from 'socket.io-client'

import './css/prosemirror.css'
import './css/site.css'
import './css/menu.css'
import './css/example-setup.css'
import './css/comments.css'
import './css/dlink.scss'

import {EditorConnection} from './collab/client/collab'
const {Reporter} = require("./collab/client/reporter")
import {NOTE} from '../ItemsTree/constants'
import {selectSelectedItem} from '../ItemsTree/selectors'
// import {selectSelectedNote} from '../DEPRECATED/Items/selectors'

// const socket = IO('http://localhost:5555')

let connection = null
class Editor extends React.Component {
    constructor(props) {
        super(props)
        this.props = props
        this.connection = null
    }

    render() {
        return (
            <div>
                {/*<div id="editor" ref={(c) => this._editor1 = c}></div>
                 <div ref={(c) => this._editor2 = c}></div>
                 <div style={{display: "none"}} ref={(c) => this._editorContent = c}>
                 <h1>Using ProseMirror</h1>

                 <p>This is editable text. Focus it and start typing.</p>
                 </div>*/}
                <div id="editor"></div>
            </div>
        )
    }

    componentWillMount() {

    }

    componentDidMount() {


        const report = new Reporter()
        if (this.connection) this.connection.close()
        this.connection = window.connection = new EditorConnection(report, "/docs/" + this.getEditorId(this.props.selectedNote))
        // connection.request.then(() => connection.view.editor.focus())

        // let content = this._editorContent
        // content.style.display = "none"
        //
        // let doc = DOMParser.fromSchema(this.schema).parse(content)
        // const authority = new Authority(doc)
        //
        //
        // let editorView1 = collabEditor(authority, this._editor1)
        // let editorView2 = collabEditor(authority, this._editor2)

    }

    componentDidUpdate(prevProps, prevState) {
        this.connection.url = "/docs/" + this.getEditorId(this.props.selectedNote)
        this.connection.closeRequest()
        // this.connection.dispatch({type: "restart"})
        this.connection.start()
    }

    getEditorId(note) {
        let editorId
        if (note) {
            // editorId = note[noteId].editorId
            editorId = note.id
        }
        return editorId || 'dummy'
    }
}


Editor.propTypes = {
    selectedNote: PropTypes.object
}

const mapStateToProps = createStructuredSelector({
    selectedNote: selectSelectedItem(NOTE)
})

export default connect(mapStateToProps)(Editor)
