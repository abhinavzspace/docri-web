const {Schema} = require("prosemirror-model")
const {schema: base} = require("prosemirror-schema-basic")
const {addListNodes} = require("prosemirror-schema-list")
const {InputRule} = require("prosemirror-inputrules")
const crel = require('crel')

// TODO: put in config
const websiteUrl = "http://localhost:4200/"


const dLinkExternal = {
    attrs: {
        type: {default: "external-link"},
        dhref: {default: "http://docri.com"},
        dtitle: {default: "default title"},
    },
    toDOM: node => crel("span",
        {
            "link-type": node.attrs.type,
            "link-href": node.attrs.dhref,
            "link-title": node.attrs.dtitle,
            contenteditable: false,
            "class": "d-" + node.attrs.type
        },
        crel("span",
            {},
            node.attrs.dtitle
        ),
        crel("a",
            {
                href: node.attrs.dhref,
                title: node.attrs.dtitle,
                target: "_blank",
                "class": "glyphicon glyphicon-new-window",
                "aria-hidden": "true"
            }
        )
    ),
    parseDOM: [{
        tag: "span[link-type]",
        getAttrs: dom => {
            let type = dom.getAttribute("link-type")
            let dhref = dom.getAttribute("link-href")
            let dtitle = dom.getAttribute("link-title")
            return {type, dhref, dtitle}
        }
    }],

    inline: true,
    group: "inline",
    draggable: true
}

const dMentionUser = {
    attrs: {
        type: {default: "d-mention-user"},
        dMentionDataUsername: {default: "someUserName"},
    },
    toDOM: node => crel("span",
        {
            "d-mention-type": node.attrs.type,
            "d-mention-data-username": node.attrs.dMentionDataUsername,
            contenteditable: false,
            "class": node.attrs.type
        },
        crel("a",
            {
                href: websiteUrl + node.attrs.dMentionDataUsername,
                title: node.attrs.dMentionDataUsername,
            },
            crel("span",
                {},
                "@"
            ),
            crel("span",
                {},
                node.attrs.dMentionDataUsername
            )
        )
    ),
    parseDOM: [{
        tag: "span[d-mention-type=d-mention-user]",
        getAttrs: dom => {
            let type = 'd-mention-user'
            let dMentionDataUsername = dom.getAttribute("d-mention-data-username")
            return {type, dMentionDataUsername}
        }
    }],

    inline: true,
    group: "inline",
    draggable: true
}

const dMentionItem = {
    attrs: {
        type: {default: "d-mention-item"},
        dMentionDataUsername: {default: ""},
        dMentionDataItemType: {default: ""},
        dMentionDataItemId: {default: ""},
    },
    toDOM: node => {
        let type  // convert type from single character to word. Only for displaying.
        switch (node.attrs.dMentionDataItemType) {
            case "d":
                type = "docri"
                break
            case "f":
                type = "folder"
                break
            case "n":
                type = "note"
                break
            default:
                throw new Error("This type is not applicable")
        }

        const urlPart = node.attrs.dMentionDataUsername +
            "/" + type +
            "/" + node.attrs.dMentionDataItemId
        return crel("span",
            {
                "d-mention-type": node.attrs.type,
                "d-mention-data-username": node.attrs.dMentionDataUsername,
                "d-mention-data-item-type": node.attrs.dMentionDataItemType,
                "d-mention-data-item-id": node.attrs.dMentionDataItemId,
                contenteditable: false,
                "class": node.attrs.type
            },
            crel("a",
                {
                    href: websiteUrl + urlPart,
                    title: websiteUrl + urlPart,
                },
                crel("span",
                    {},
                    urlPart
                )
            )
        )
    },
    parseDOM: [{
        tag: "span[d-mention-type=d-mention-item]",
        getAttrs: dom => {
            let type = 'd-mention-item'
            let dMentionDataUsername = dom.getAttribute("d-mention-data-username")
            let dMentionDataItemType = dom.getAttribute("d-mention-data-item-type")
            let dMentionDataItemId = dom.getAttribute("d-mention-data-item-id")
            return {type, dMentionDataUsername, dMentionDataItemType, dMentionDataItemId}
        }
    }],

    inline: true,
    group: "inline",
    draggable: true
}


const schema = new Schema({
    nodes: addListNodes(base.nodeSpec.append({dMentionUser, dMentionItem, dLinkExternal})
        , "paragraph block*", "block"),
    marks: base.markSpec
})


// !![dtitle](dhref)        <-- This is the format for external-link
const dLinkExternalInputRule = new InputRule(new RegExp("\\!\\!\\[(.*?)\\]\\((.+?)\\)$"), (state, match, start, end) => {
    let y = schema.nodes.dLinkExternal.create({type: 'external-link', dtitle: match[1], dhref: match[2]})
    let x = state.tr.replaceWith(start, end, y)
    return x
})


// @[dtitle](dhref)        <-- This is the format for internal-link
// const dLinkInternalInputRule = new InputRule(new RegExp("\\@\\[(.*?)\\]\\((.+?)\\)$"), (state, match, start, end) => {
//     let y = schema.nodes.dLinkInternal.create({type: 'internal-link', dtitle: match[1], dhref: match[2]})
//     let x = state.tr.replaceWith(start, end, y)
//     return x
// })

exports.schema = schema

exports.dLinkExternalInputRule = dLinkExternalInputRule

// exports.dLinkInternalInputRule = dLinkInternalInputRule

// exports.schema = new Schema({
//   nodes: addListNodes(base.nodeSpec, "paragraph block*", "block"),
//   marks: base.markSpec
// })
