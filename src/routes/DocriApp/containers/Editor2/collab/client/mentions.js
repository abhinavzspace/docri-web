/**
 * Created by abhinav on 16/12/16.
 */
const debug = require('debug')('app:mentionPlugin')

const {Plugin} = require("prosemirror-state")
const crel = require("crel")
const {Decoration, DecorationSet} = require("prosemirror-view")
import {getDs, getToken} from '../../../utils/connectors'
import fetch from 'isomorphic-fetch'
import conf from '../../../../../../../conf-abhinav'
const {schema} = require("../schema")

const authEndpoint = conf.authEndpoint

// TODO: Clicking outside widget on the Editor2 content should removeDropdownMention

// TODO: on selection of item, replace text with a custom-node of type d-mention-user or d-mention-internal-link

let isDropdownActive = false



const searchAll = (text) => {
    let myHeaders = new Headers()
    myHeaders.append('Authorization', 'Bearer ' + getToken())
    const url = `${authEndpoint}/search/${text}`
    const fetchResult = fetch(url, {
        method: 'GET',
        headers: myHeaders,
        mode: 'cors',
        cache: 'default'
    })
        .then(res => res.json())
        // .then(json => json.users.map(user => user.username))

    return fetchResult
}



class MentionView {
    constructor(options, from, mentions, selectedIndex, text) {
        this.options = options
        this.from = from
        this.mentions = mentions
        this.selectedIndex = selectedIndex
        this.text = text
    }

    selectItem(index) {
        const fromPos = this.from - 1          // To include "@" before text
        const toPos = this.from + this.text.length
        const mention = this.mentions[index] //todo
        this.createMention(mention, fromPos, toPos)
    }

    createMention(mention, fromPos, toPos) {
        let dMention
        if (mention.type) {  // is item
            const obj = {
                dMentionDataUsername: mention.username,
                dMentionDataItemType: mention.type,
                dMentionDataItemId: mention.id
            }
            dMention = schema.nodes.dMentionItem.create(obj)
        } else if (mention.username) {      // is user
            dMention = schema.nodes.dMentionUser
                .create({dMentionDataUsername: mention.username})
        }

        this.options.dispatch(this.options.getState().tr.replaceWith(fromPos, toPos, dMention))
    }

    extractText(m) {
        let text

        if (m.type == 'd') text = m.name + ' (docri)'
        else if (m.type == 'f') text = m.name + ' (folder)'
        else if (m.type == 'n') text = m.name + ' (note)'
        else if (m.username) text = m.username
        else throw new Error('Wrong results returned from server. This type does not exist.')

        return text
    }

    render() {
        const lis = this.mentions.map((m, ind) => {
            const selectedClass = ind == this.selectedIndex ? 'selected' : ''
            const attrs = {
                "class": "commentText " + selectedClass,
                "data-key": ind
            }

            const text = this.extractText(m)
            const li = crel('li', attrs, text)
            li.addEventListener('mouseenter', ev => {
                const indexStr = ev.target.getAttribute('data-key')
                const index = parseInt(indexStr, 10)
                const obj = {type: 'changeDropdownMention', how: 'newIndex', index}
                this.options.dispatch(this.options.getState().tr.setMeta(mentionPlugin, obj))
            })
            li.addEventListener('mousedown', ev => {
                const indexStr = ev.target.getAttribute('data-key')
                const index = parseInt(indexStr, 10)
                debug('selectItem mousedown, index', index)
                this.selectItem(index)
            })

            return li
        })
        const ul = crel('ul', {"class": "commentList"}, lis)
        return crel('div', {"class": "tooltip-wrapper"}, ul)
    }

    dropdown() {
        const mentionsDOM = this.render()
        const deco = Decoration.widget(this.from, mentionsDOM)
        const decoSet = DecorationSet.create(this.options.getState().doc, [deco])
        return decoSet
    }
}


const extractMentions = (results) => {
    if (results.users.length == 0 && results.docries.length == 0 && results.folders.length == 0 && results.notes.length == 0) {
        return null
    }

    let showResults = []
    if (results.users.length > 0) showResults = showResults.concat(results.users)
    if (results.docries.length > 0) showResults = showResults.concat(results.docries)
    if (results.folders.length > 0) showResults = showResults.concat(results.folders)
    if (results.notes.length > 0) showResults = showResults.concat(results.notes)

    return showResults
}


class MentionState {
    constructor(options, decos, from, mentions, selectedIndex, text) {
        this.options = options
        this.decos = decos || DecorationSet.empty
        this.from = Number.isInteger(from) ? from : null
        this.mentions = mentions || null
        this.selectedIndex = Number.isInteger(selectedIndex) ? selectedIndex : null
        this.text = text || null
    }

    getMentions(text, from) {
        // consolidate and show result in Decoration.widget --> DONE

        searchAll(text)
            .then(results => {
                const obj = {type: 'createDropdownMention', results, from, text}
                this.options.dispatch(this.options.getState().tr.setMeta(mentionPlugin, obj))
            })

    }

    removeDropdown() {
        isDropdownActive = false
        return new MentionState(this.options, DecorationSet.empty)
    }

    apply(tr) {
        let action = tr.getMeta(mentionPlugin), actionType = action && action.type
        if (!action) return this
        let base = this

        if (actionType == "initDropdownMention") {
            debug('initDropdownMention', action)
            if (action.text) this.getMentions(action.text, action.from)
            // TODO: Nothing gets rendered when " @" is typed, though "initDropdownMention" is called.
            else debug('Handle the case for " @"')
            return this
        }

        if (action.type == "createDropdownMention") {
            debug('createDropdownMention')

            if (!action.results) {
                return this
            }

            const mentions = extractMentions(action.results)
            if (mentions) {
                const from = action.from
                const selectedIndex = 0
                const text = action.text
                isDropdownActive = true
                const view = new MentionView(base.options, from, mentions, selectedIndex, text)
                const decoSet = view.dropdown()
                return new MentionState(base.options, decoSet, from, mentions, selectedIndex, text)
            } else {
                return this.removeDropdown()
            }
            return this
        }

        if (action.type == "removeDropdownMention") {
            debug('removeDropdownMention')
            return this.removeDropdown()
        }

        if (action.type == "changeDropdownMention") {
            let index
            switch (action.how) {
                case 'goDown':
                    // refresh selectedIndex if exceeds mentions.length
                    if (this.selectedIndex + 1 >= this.mentions.length) index = 0
                    // Normal increment
                    else index = this.selectedIndex + 1
                    break
                case 'goUp':
                    if (this.selectedIndex == 0) index = this.mentions.length - 1
                    // Normal increment
                    else index = this.selectedIndex - 1
                    break
                case 'newIndex':
                    if (Number.isInteger(action.index)) index = action.index
                    else throw new Error('action.index must be provided for this action.')
                    break
                default:
                    throw new Error('ERROR: this case does not exist')
            }
            const view = new MentionView(base.options, this.from, this.mentions, index, this.text)
            const decoSet = view.dropdown()
            return new MentionState(base.options, decoSet, this.from, this.mentions, index, this.text)
        }
        return this
    }

    static init(config, options) {
        // let decos = config.comments.comments.map(c => deco(c.from, c.to, new Comment(c.text, c.id)))
        return new MentionState(options, DecorationSet.empty)
    }
}

let mentionPlugin
const mentionPluginGenerator = (options) => {
    mentionPlugin = new Plugin({
        state: {
            init: (config) => MentionState.init(config, options),
            apply(tr, prev) {
                return prev.apply(tr)
            }
        },
        props: {
            decorations(state) {
                return this.getState(state).decos
            },
            handleClick: (view, pos, event) => runCheck(view, event, options),
            handleKeyPress: (view, event) => runCheck(view, event, options),
            // handleKeyDown: (view, event) => navigateMentionDropdown(view, event)
        }
    })

    return mentionPlugin
}
exports.mentionPluginGenerator = mentionPluginGenerator




// export const goDownDropdown = (state, onAction) => {
//     if (!isDropdownActive) return false
//
//     onAction({type: 'changeDropdownMention', how: 'goDown'})
//     return true
// }
// export const goUpDropdown = (state, onAction) => {
//     if (!isDropdownActive) return false
//
//     onAction({type: 'changeDropdownMention', how: 'goUp'})
//     return true
// }
// // FIXME: not working
// export const selectItemDropdown = (state, onAction) => {
//     if (!isDropdownActive) return false
//
//     onAction({type: 'selectItemDropdownMention'})
//     return true
// }
// // FIXME: should remove dropdown on single "ESC" key (right now, need to press "ESC" key two times.)
// // SOLUTION: "select parent node" command in menuToolbar might be interfering
// export const escDropdown = (state, onAction) => {
//     if (!isDropdownActive) return false
//
//     onAction({type: 'removeDropdownMention'})
//     return false        // Let the Editor2 process further
// }


// TODO: Throwing errors
function navigateMentionDropdown(view, event) {
    if (!event.key) return false

    let obj = null
    let bool = false    // For all the cases
    if (event.keyCode == 40) { // down key
        debug('changeDropdownMention')
        obj = {type: 'changeDropdownMention', how: 'goDown'}
        bool = true
    } else if (event.keyCode == 38) { // up key
        debug('changeDropdownMention')
        obj = {type: 'changeDropdownMention', how: 'goUp'}
        bool = true
    } else if (event.keyCode == 13) { // enter key
        debug('selectItemDropdownMention')
        obj = {type: 'selectItemDropdownMention'}       // TODO: not implemented
        bool = true
    } else if (event.keyCode == 27) { // esc key
        debug('removeDropdownMention')
        // FIXME: should remove dropdown on single "ESC" key (right now, need to press "ESC" key two times.)
        obj = {type: 'removeDropdownMention'}
        bool = false    // Let the Editor2 process further
    }


    if (obj) view.dispatch(view.state.tr.setMeta(mentionPlugin, obj))

    return bool
}



function runCheck(view, event, options) {

    const state = options.getState()
    let sel = state.selection
    if (!sel.empty) return false

    const data = checkMentions(state)
    if (data) {
        debug('text -->', data.text)
        options.dispatch(options.getState().tr.setMeta(mentionPlugin, {type: "initDropdownMention", text: data.text, from: data.from}))
    } else {
        if (isDropdownActive)
            options.dispatch(options.getState().tr.setMeta(mentionPlugin, {type: "removeDropdownMention"}))
    }

    // return navigateMentionDropdown(view, event)
    return false
}


let reverse_mention = /^([a-zA-Z0-9 _!#$%&*-]*[a-zA-Z0-9]?)[@＠]/g
function checkMentions(node) {
    const selectionNodePos = node.selection.$from.before()
    const cursorPos = node.selection.$from.pos

    let textToSearch = node.doc.textBetween(selectionNodePos, cursorPos)
    const reversedText = textToSearch.split("").reverse().join("")      // reverse the string

    let m = reversedText.match(reverse_mention)

    console.log('m', m)

    if (m) {
        const reversedBackResult = m[0].split("").reverse().join("")
        const text = reversedBackResult.substring(1, reversedBackResult.length)     // remove '@'
        const from = cursorPos - reversedBackResult.length

        return {text, from}
    }

    return null
}
