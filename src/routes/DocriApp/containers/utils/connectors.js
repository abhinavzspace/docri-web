/**
 * Created by abhinav on 16/12/16.
 */

import {getItem, setItem, removeItem, hasItem} from '../../inMemoryData'
import {localStorageGetItem, localStorageSetItem, localStorageRemoveItem} from '../../localStorageAdapter'


// DS
const DS = 'ds'
export const setDs = (ds) => setItem(DS, ds)
export const getDs = () => getItem(DS)
export const hasDs = () => hasItem(DS)
export const removeDs = () => removeItem(DS)


// token
const TOKEN = 'TOKEN'
export const setToken = (token) => localStorageSetItem(TOKEN, token)
export const getToken = () => localStorageGetItem(TOKEN)
export const removeToken = () => localStorageRemoveItem(TOKEN)


// username
const USERNAME = 'USERNAME'
export const setUsername = (username) => localStorageSetItem(USERNAME, username)
export const getUsername = () => localStorageGetItem(USERNAME)
export const removeUsername = () => localStorageRemoveItem(USERNAME)


// ds-backup
const DS_BACKUP = 'ds-backup'
export const setDsBackup = (backup) => localStorageSetItem(DS_BACKUP, backup)
export const getDsBackup = () => localStorageGetItem(DS_BACKUP)
export const removeDsBackup = () => localStorageRemoveItem(DS_BACKUP)
