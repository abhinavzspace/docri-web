/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {ListGroupItem, ListGroup} from 'react-bootstrap'
import {Link} from 'react-router'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
import {getUsername} from '../utils/connectors'
import {selectSelectedItem, selectItems} from '../ItemsTree/selectors'
import {NOTE} from '../ItemsTree/constants'
import {createEntryFor} from '../ItemsTree/actionCreators'
import './styles.scss'

const Notes = ({items, selectedItem, createNote}) => {
    let input

    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        createNote(input.value)
        input.value = ''
    }
    const displayNotes = () => {
        if (!items) {
            return
        }

        let notesLis = []
        for (let note of items) {
            const selectedClass = selectedItem && ( note.id === selectedItem.id ) ? 'selected' : ''
            notesLis.push(
                <Link to={`/${getUsername()}/note/${note.id}`} key={note.id} className={`notes-item d-list__item ${selectedClass}`}>
                    <div
                        key={note.id}
                        className="notes-list-group-item">
                        {note.name}
                    </div>
                </Link>
            )
        }
        return notesLis
    }
    return (
        <div>
            <div className="notes-list d-list">
                <div key={0}> {'Notes'} </div>
                {displayNotes()}
            </div>
            <form className="form-inline" onSubmit={submit} style={{display: 'flex'}}>
                <div className="form-group form-group-sm">
                    <input type="text" className="form-control" ref={node => {
                        input = node
                    }} placeholder="Note Name" style={{width: 100 + '%'}}/>
                </div>
                <button type="submit" className="btn btn-default btn-s">+</button>
            </form>
        </div>
    )
}

Notes.propTypes = {
    createNote: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    selectedItem: PropTypes.object
}

const mapStateToProps = createStructuredSelector({
    selectedItem: selectSelectedItem(NOTE),
    items: selectItems(NOTE)
})

const mapDispatchToProps = {
    createNote: createEntryFor(NOTE),
}

export default connect(mapStateToProps, mapDispatchToProps)(Notes)
// export default Notes
