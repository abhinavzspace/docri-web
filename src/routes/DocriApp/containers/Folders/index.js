/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {Dropdown, MenuItem, Glyphicon} from 'react-bootstrap'
import {Link} from 'react-router'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
import {getUsername} from '../utils/connectors'
import {selectSelectedItem, selectItems} from '../ItemsTree/selectors'
import {FOLDER} from '../ItemsTree/constants'
import {createEntryFor} from '../ItemsTree/actionCreators'
import './styles.scss'

const Folders = ({items, selectedItem, createFolder}) => {
    let input

    const dropdown = (id) => {
        return (
            <Dropdown id={`dropdown-folder-${id}`}>
                <Dropdown.Toggle noCaret bsSize="xsmall">
                    <Glyphicon glyph="option-vertical"/>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <MenuItem eventKey="1">Rename</MenuItem>
                </Dropdown.Menu>
            </Dropdown>
        )
    }

    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        createFolder(input.value)
        input.value = ''
    }
    const displayFolders = () => {
        if (!items) {
            return
        }

        let foldersLis = []
        for (let folder of items) {
            const selectedClass = selectedItem && ( folder.id === selectedItem.id ) ? 'selected' : ''
            foldersLis.push(
                <Link to={`/${getUsername()}/folder/${folder.id}`} key={folder.id}
                      className={`folders-item d-list__item ${selectedClass}`}>
                    <div
                        key={folder.id}
                        className="folders-list-group-item">
                        {folder.name}
                    </div>
                </Link>
            )
        }
        return foldersLis
    }
    return (
        <div>
            <div className="folders-list d-list">
                <div key={0}> {'Folders'} </div>
                {displayFolders()}
            </div>
            <form className="form-inline" onSubmit={submit} style={{display: 'flex'}}>
                <div className="form-group form-group-sm">
                    <input type="text" className="form-control" ref={node => {
                        input = node
                    }} placeholder="Folder Name" style={{width: 100 + '%'}}/>
                </div>
                <button type="submit" className="btn btn-default btn-s">+</button>
            </form>
        </div>
    )
}

Folders.propTypes = {
    createFolder: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    selectedItem: PropTypes.object
}

const mapStateToProps = createStructuredSelector({
    selectedItem: selectSelectedItem(FOLDER),
    items: selectItems(FOLDER)
})

const mapDispatchToProps = {
    createFolder: createEntryFor(FOLDER),
}

export default connect(mapStateToProps, mapDispatchToProps)(Folders)
// export default Folders
