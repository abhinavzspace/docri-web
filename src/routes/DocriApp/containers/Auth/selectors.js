/**
 * Created by abhinav on 07/10/16.
 */
import {createSelector} from 'reselect'


export const selectAuth = (state) => state.get('auth')

export const selectUsername = createSelector(
    selectAuth,
    (authState) => authState.get('username')
)

export const selectAuthStatus = createSelector(
    selectAuth,
    (authState) => authState.get('status')
)

export const selectAuthObject = createSelector(
    selectAuth,
    (authState) => authState.toJS()
)
