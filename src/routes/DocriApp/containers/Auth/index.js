/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
import {tryLogin, tryLogout} from './actionCreators'
// import {loginUser, logoutUser} from './thunks'
import {selectAuthObject} from './selectors'
import C from './constants'

const Auth = ({auth, tryLogout, tryLogin}) => {
    let usernameInput
    let passwordInput
    const submit = e => {
        e.preventDefault()
        if (!usernameInput.value.trim() || !passwordInput.value.trim()) {
            return
        }
        tryLogin(usernameInput.value, passwordInput.value)
        usernameInput.value = ''
        passwordInput.value = ''
    }
    switch (auth.status) {
        case C.AUTH_LOGGED_IN:
            return (
                <div>
                    <span>Logged in as {auth.username}.</span>
                    {" "}
                    <button onClick={tryLogout}>Log out</button>
                </div>
            )
        case C.AUTH_AWAITING_RESPONSE:
            return (
                <div>
                    <button disabled>authenticating...</button>
                </div>
            )
        default:
            return (
                <form className="form-inline" onSubmit={submit}>
                    <div className="form-group">
                        <label htmlFor="username">Username</label>
                        <input type="text" className="form-control" id="username" ref={node => {
                            usernameInput = node
                        }} placeholder="Username"/>
                        <label htmlFor="password">Password</label>
                        <input type="password" className="form-control" id="password" ref={node => {
                            passwordInput = node
                        }} placeholder="Password"/>
                    </div>
                    <button type="submit" className="btn btn-default">Login</button>
                </form>
            )
    }

}

Auth.propTypes = {
    tryLogin: PropTypes.func.isRequired,
    tryLogout: PropTypes.func.isRequired,
    auth: PropTypes.shape({
        username: PropTypes.string,
        status: PropTypes.string
    }).isRequired
}

const mapDispatchToProps = {
    tryLogin,
    tryLogout,
};

const mapStateToProps = createStructuredSelector({
    auth: selectAuthObject
})

export default connect(mapStateToProps, mapDispatchToProps)(Auth)
