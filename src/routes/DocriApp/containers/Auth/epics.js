/**
 * Created by abhinav on 30/11/16.
 */

import conf from '../../../../../conf-abhinav'
import deepstream from 'deepstream.io-client-js'
import fetch from 'isomorphic-fetch'
import {Observable} from 'rxjs'
import {combineEpics} from 'redux-observable'
import C from './constants'
// import {listenDocries} from '../Docries/actionCreators'
import {authLogin, authLogout} from './actionCreators'
// import {reInit as docriesReInit} from '../Docries/actionCreators'
// import {reInit as foldersReInit} from '../Folders/actionCreators'
// import {reInit as notesReInit} from '../Notes/actionCreators'
// import {stopListeningFolders} from '../Folders/thunks'
// import {stopListeningNotes} from '../Notes/thunks'
import {getToken, setToken, removeToken
    , getUsername, removeUsername, setUsername} from '../utils/connectors'
// import {reInit} from '../DEPRECATED/Items/actionCreators'
import { push } from 'react-router-redux'

const authEndpoint = conf.authEndpoint


const fetchToken = (action) => {
    const authData = {username: action.username, password: action.password}
    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    return Observable.create(o => {
        fetch(`${authEndpoint}/login`, {
            method: 'POST',
            headers: myHeaders,
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify(authData)
        })
            .then(response => response.json())
            .then(json => json.jwt)
            .then(jwt => {
                o.next(jwt);
                o.complete();
            }).catch(err => o.error(err))
        // TODO: Display login fail error message.
    })

}


// const loginDsPromise = (token) => new Promise((resolve, reject) => {
//
//     if (hasDs()) throw new Error('ds already present in memory. And another login call made for it.')
//
//     console.log('----loginDsPromise----')
//     let ds
//     if (!ds) {
//         // Keep on connecting, when offline
//         ds = deepstream(deepstreamEndpoint, {
//             maxReconnectAttempts: 10000,
//             reconnectIntervalIncrement: 500,    // 500 milli secs
//             maxReconnectInterval: 10000     // 10 secs
//         })
//     }
//     ds.login({token}, (success, data) => {
//         if (success === true) {
//             console.log('login successful, Data: ', data)
//         } else {
//             // dispatch(logoutUser());
//             console.log('login failed with data', data)
//             reject()
//         }
//     })
//     resolve(ds)
// })


// const watchConnectionState = (ds, username, token, afterLogin) => Observable.create(o => {
//     ds.on('connectionStateChanged', connectionState => {
//         console.log('connectionStateChanged', connectionState)
//         switch (connectionState) {
//             case 'CLOSED':
//             case 'ERROR':
//                 // o.next(authLogout())
//                 // TODO: switch to "localForage"
//                 setDsBackup(JSON.stringify(ds._connection._queuedMessages))
//                 break
//             case 'OPEN':
//                 o.next(authLogin())                     // dispatch
//
//                 if (afterLogin) {
//                     // afterLogin is an action object
//                     o.next(afterLogin)                  // dispatch
//                 }
//
//                 var storedMessages = getDsBackup();
//                 if (storedMessages) {
//                     var messages = JSON.parse(storedMessages);
//                     ds._connection._queuedMessages = ds._connection._queuedMessages.concat(messages);
//                     removeDsBackup()
//                 }
//                 break
//         }
//     })
//     window.dsHandle = ds
//     ds.on('error', (error, event, topic) =>
//         console.log('deepstream error -->', {error, event, topic})
//     )
//     return () => {
//         console.log('ds.close()')
//         // ds.close()
//     }
// })


export const loginUserEpic = (action$, {getState}) =>
    action$.ofType(C.TRY_LOGIN)
        .do(origAction => setUsername(origAction.username))
        .switchMap(origAction =>
            fetchToken(origAction)
                .takeUntil(action$.ofType(C.AUTH_LOGOUT))
                .do(token => setToken(token))   // For Editor2 --> http.js
                .map(() => authLogin())     // dispatch
                // .switchMap(token =>
                //     doDsLoginWithToken(token, origAction.username)
                // )
        )

// export const loginDsTokenPresentEpic = (action$) =>
//     action$.ofType(C.LOGIN_TOKEN_PRESENT)
//         .map(action => ({token: getToken(), username: getUsername(), afterLogin: action.afterLogin}))
//         .switchMap(({token, username, afterLogin}) =>
//             doDsLoginWithToken(token, username, afterLogin)
//         )
//
// const doDsLoginWithToken = (token, username, afterLogin) =>
//     Observable.from(loginDsPromise(token))
//         .do(ds => setDs(ds))
//         .mergeMap(ds => watchConnectionState(ds, username, token, afterLogin))


// TODO
export const logoutUserEpic = (action$, {getState}) =>
    action$.ofType(C.TRY_LOGOUT)
    // .map(() => selectDs(getState()))
    //     .map(() => getDs().close())
        // Real action for LOGOUT will fire in connectionStateChanged event.
        // .do(() => removeDs())
        .do(() => removeToken())
        .do(() => removeUsername())
        .switchMap(() =>
            Observable.merge(
                Observable.of(reInit())
                // TODO: Should all the records and handles be removed individually through switchMap?
            )
        )


export const replaceUrlEpic = (action$) =>
    action$.ofType(C.CHANGE_URL)
        .do(x => console.log('CHANGE_URL action', x))
        .map(action => push(action.url))            // dispatch


// export const afterLoginEpic = (action$, {getState}) =>
//     action$.ofType(C.AUTH_LOGIN)
//         .map(listenDocries)


export default combineEpics(loginUserEpic, logoutUserEpic, replaceUrlEpic)
