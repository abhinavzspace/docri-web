import {fromJS, Map} from 'immutable'

import C from './constants'


// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    [C.AUTH_OPEN]: (state, action) => {
        return fromJS({
            status: C.AUTH_AWAITING_RESPONSE,
            // username: null,
            // token: null,
            // ds: null
        })
    },

    [C.AUTH_LOGIN]: (state, action) => {
        return fromJS({
            status: C.AUTH_LOGGED_IN,
            // username: action.username,
            // token: action.token,
            // ds: action.ds
        })
    },

    [C.AUTH_LOGOUT]: (state, action) => {
        return fromJS({
            status: C.AUTH_ANONYMOUS,
            // username: null,
            // token: null,
            // ds: null
        })
    }

}

const initialState = fromJS({
    // username: null,
    // token: null,
    status: C.AUTH_ANONYMOUS,
    // ds: null
})
function authReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default authReducer
