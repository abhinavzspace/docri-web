/**
 * Created by abhinav on 13/10/16.
 */

import C from './constants'

export const tryLogin = (username, password) => {
    return {
        type: C.TRY_LOGIN,
        username,
        password
    }
}

export const tryLoginTokenPresent = (afterLogin) => { return { type: C.LOGIN_TOKEN_PRESENT, afterLogin } }

export const tryLogout = () => { return { type: C.TRY_LOGOUT } }

export const authLogin = () => {
    return {
        type: C.AUTH_LOGIN,
        // username,
        // token,
        // ds
    }
}

export const authLogout = () => { return { type: C.AUTH_LOGOUT } }
