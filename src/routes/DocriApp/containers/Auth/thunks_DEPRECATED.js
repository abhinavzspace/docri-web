/**
 * Created by abhinav on 14/10/16.
 */

import fetch from 'isomorphic-fetch'
// import * as firebase from 'firebase'
import C from './constants'
// import {auth} from '../../firebaseApp'
import {selectAuth} from './selectors'
import {listenToDocries, selectFirstDocriThunk, stopListeningDocries} from '../Docries/thunks'
import {authLogin, authLogout, authOpen} from './actionCreators'
import {reInit as docriesReInit} from '../Docries/actionCreators'
import {reInit as foldersReInit} from '../Folders/actionCreators'
import {reInit as notesReInit} from '../Notes/actionCreators'
import {stopListeningFolders} from '../Folders/thunks'
import {stopListeningNotes} from '../Notes/thunks'

import {getDsClient, removeDsClient} from '../../deepstreamApp'

import {Observable} from 'rxjs'


let username_auth

export const listenToAuth = () => {
    return (dispatch, getState) => {
        getDsClient().on('connectionStateChanged', connectionState => {
            console.log('connectionState', connectionState)

            switch (connectionState) {
                case 'CLOSED':
                    dispatch(logoutUser());
                    break;
                case 'OPEN':
                    dispatch(authLogin(username_auth))
                    dispatch(listenToDocries()).then(() => {
                        dispatch(selectFirstDocriThunk())
                    })
                    break;
            }
            // will be called with 'CLOSED' once the connection is successfully closed.
        })

        // FIREBASE
        // auth.onAuthStateChanged((authData) => {
        //     if (authData) {
        //         dispatch(authLogin(authData.uid, authData.email));
        //         dispatch(listenToDocries()).then(() => {
        //             dispatch(selectFirstDocriThunk())
        //         })
        //     } else {
        //         const authState = selectAuth(getState())
        //         if (authState.status !== C.AUTH_ANONYMOUS) {
        //             dispatch(authLogout());
        //         }
        //     }
        // });
    };
};

export const loginUser = (username, password) => {
    return (dispatch) => {
        dispatch(authOpen())
        dispatch(listenToAuth())

        // Save username for using in listenToAuth()
        username_auth = username

        /**
         * First hit docri-auth server with username and password, token will be received.
         * Then hit deepstream with token.
         */
        const authData = {username, password}
        let myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        fetch('http://localhost:4500/auth/login', {
            method: 'POST',
            headers: myHeaders,
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify(authData)
        })
            .then(response => response.json())
            .then(json => {

                if (!json.token) {
                    // TODO: Show message to user.
                    dispatch(logoutUser());
                    console.error('Token is not present. Login failed.')
                    return
                }

                getDsClient().login({token: json.token}, (success, data) => {
                    if (success === true) {
                        console.log('login successful, Data: ', data)
                        // dispatch(listenToDocries()).then(() => {
                        //     dispatch(selectFirstDocriThunk())
                        // })

                        // const docriesList = client.record.getList('docriesOf/' + 'abhinav')
                        // docriesList.whenReady(list => {
                        //
                        // })
                    } else {
                        dispatch(logoutUser());
                        console.log('login failed with data', data)
                    }
                })
            })
            .catch((error) => {
                console.error('There has been a problem with Login: ' + error.message);
            });



        // FIREBASE
        // auth.signInWithEmailAndPassword(email, password).catch(err => {
        //     // dispatch({
        //     //     type: C.FEEDBACK_DISPLAY_ERROR,
        //     //     error: `Login failed! ${error}`
        //     // });
        //     dispatch(logoutUser())
        // })


        // const provider = new firebase.auth.FacebookAuthProvider();
        // auth.signInWithPopup(provider)
        //     .catch((error) => {
        //         dispatch({
        //             type: C.FEEDBACK_DISPLAY_ERROR,
        //             error: `Login failed! ${error}`
        //         });
        //         dispatch({ type: C.AUTH_LOGOUT });
        //     });
    };
};

export const logoutUser = () => {
    return (dispatch) => {
        stopListeningDocries()
        stopListeningFolders()
        stopListeningNotes()
        dispatch(docriesReInit())
        dispatch(foldersReInit())
        dispatch(notesReInit())
        dispatch(authLogout())

        // FIXME: discard() should return promise?
        setTimeout(() => removeDsClient(), 2000)
    };
};
