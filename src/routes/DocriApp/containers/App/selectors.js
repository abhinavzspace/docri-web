/**
 * Created by abhinav on 18/10/16.
 */

import {createSelector} from 'reselect'


export const selectApp = (state, {params}) => {
    return ({
        appState: state.getIn(['user', 'app']),
        params
    })
}

export const selectFoldersOpen = createSelector(
    selectApp,
    ({appState}) => appState.get('foldersOpen')
)

export const selectNotesOpen = createSelector(
    selectApp,
    ({appState}) => appState.get('notesOpen')
)

export const selectRouteParamsFromUrl = createSelector(
    selectApp,
    ({params}) => ({routeParams: params})
)

export const selectRouteParamsFromState = (state) => ({
    urlParams: state.getIn(['user', 'app', 'urlParams']).toJS()
})
