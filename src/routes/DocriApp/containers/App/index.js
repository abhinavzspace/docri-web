/**
 * Created by abhinav on 12/10/16.
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {createStructuredSelector} from 'reselect'

import './styles.scss'
// import Header from '../../components/Header'
// import Sidebar from '../../components/Sidebar'
// import Content from '../../components/Content'
import CreateDocri from '../Docries/CreateDocri'
import DisplayDocries from '../Docries/DisplayDocries'
// import Auth from '../Auth'
import Folders from '../Folders'
import Notes from '../Notes'
import {selectFoldersOpen, selectNotesOpen} from './selectors'
import {FOLDERS_TOGGLE, NOTES_TOGGLE} from './reducer'
import {selectedItemPos} from '../ItemsTree/selectors'
import {DOCRI, FOLDER, NOTE} from '../ItemsTree/constants'
// import {createEntryFor} from '../DEPRECATED/Items/actionCreators'
import {ITEM_ROUTE_CHANGE} from '../ItemsTree/constants'

// import Editor from '../Editor'
import Editor2 from '../Editor2'


class AppDocri extends React.Component {

    constructor(props) {
        super(props)
        // this.props.routeChange()
    }

    componentWillMount() {

    }

    componentWillReceiveProps(nextProps) {
        // console.log('nextProps.params.id', nextProps.params.id, 'this.props.params.id', this.props.params.id)
        // if (nextProps.params.id !== this.props.params.id) {
        //     console.log('nextProps.routeChange()')
        //     nextProps.routeChange()
        // }
    }

    loggger(obj) {
        console.log('obj', obj)
    }

    render() {
        const {toggleFolders, toggleNotes, foldersOpen, notesOpen} = this.props
        let foldersOpenClass = foldersOpen ? '' : 'app-hide-col'
        let notesOpenClass = notesOpen ? '' : 'app-hide-col'

        return (
            <div>

                <div>
                    <button type="button" onClick={() => toggleFolders()}>Toggle Folder</button>
                    <button type="button" onClick={() => toggleNotes()}>Toggle Note</button>
                    <div style={{display: 'flex'}}>
                        <DisplayDocries></DisplayDocries>
                        <CreateDocri></CreateDocri>
                    </div>
                </div>

                <div className="app-wrapper">
                    <div className={`app-folders app-transform ${foldersOpenClass}`}>
                        <Folders></Folders>
                    </div>
                    <div className={`app-notes app-transform ${notesOpenClass}`}>
                        <Notes></Notes>
                    </div>
                    <div className="app-editor">
                        <Editor2 editorId="Example"></Editor2>
                    </div>
                </div>
            </div>

        )
    }
}

AppDocri.propTypes = {
    toggleFolders: PropTypes.func.isRequired,
    toggleNotes: PropTypes.func.isRequired,
    foldersOpen: PropTypes.bool.isRequired,
    notesOpen: PropTypes.bool.isRequired,
    // routeChange: PropTypes.func.isRequired,
    // router: PropTypes.shape({
    //     push: PropTypes.func.isRequired
    // }).isRequired
}

const mapDispatchToProps = {
    toggleFolders: () => {
        return {type: FOLDERS_TOGGLE}
    },
    toggleNotes: () => {
        return {type: NOTES_TOGGLE}
    },
    // routeChange: () => ({type: ITEM_ROUTE_CHANGE})
}


const mapStateToProps = createStructuredSelector({
    foldersOpen: selectFoldersOpen,
    notesOpen: selectNotesOpen,
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppDocri))
