/**
 * Created by abhinav on 18/10/16.
 */
import {fromJS, Map} from 'immutable'

export const FOLDERS_TOGGLE = 'FOLDERS_TOGGLE'
export const NOTES_TOGGLE = 'NOTES_TOGGLE'
export const SET_ROUTE_PARAMS = 'SET_ROUTE_PARAMS'

// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    [FOLDERS_TOGGLE]: (state, action) => {
        return state.set('foldersOpen', !state.get('foldersOpen'))
    },

    [NOTES_TOGGLE]: (state, action) => {
        return state.set('notesOpen', !state.get('notesOpen'))
    },

    [SET_ROUTE_PARAMS]: (state, action) => {
        return state.set('urlParams', fromJS(action.urlParams))
    }

}

const initialState = fromJS({
    foldersOpen: true,
    notesOpen: true,
    urlParams: {}
})
function appReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default appReducer
