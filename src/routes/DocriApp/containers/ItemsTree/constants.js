/**
 * Created by abhinav on 23/12/16.
 */

export const USERNAME = 'USERNAME'
export const DOCRI = 'd'
export const FOLDER = 'f'
export const NOTE = 'n'

export const REPLACE_ENTRIES = 'REPLACE_ENTRIES'
export const CHANGE_ENTRY = 'CHANGE_ENTRY'
export const CHANGE_SELECTED_ITEM = 'CHANGE_SELECTED_ITEM'
export const RE_INIT = 'RE_INIT'
export const CREATE_ENTRY = 'CREATE_ENTRY'
export const ITEM_ROUTE_CHANGE = 'ITEM_ROUTE_CHANGE'
export const FETCH_TREE = 'FETCH_TREE'
export const SET_DOCRIES = 'SET_DOCRIES'
