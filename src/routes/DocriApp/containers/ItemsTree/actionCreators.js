/**
 * Created by abhinav on 23/12/16.
 */
import {
    REPLACE_ENTRIES, CHANGE_ENTRY, CHANGE_SELECTED_ITEM, RE_INIT,
    CREATE_ENTRY, FETCH_TREE, SET_DOCRIES
} from './constants'
import {
    DOCRI_COLL, FOLDER_COLL, NOTE_COLL
    , DOCRI, FOLDER, NOTE, USERNAME
} from '../DEPRECATED/Reaction/constants'



export const createEntryFor = (itemType) => (name) => ({type: CREATE_ENTRY, itemType, name})



export const changeSelectedItem = (itemType, itemId) => ({
    type: CHANGE_SELECTED_ITEM,
    itemType,
    itemId
})

export const setDocries = (data) => ({
    type: SET_DOCRIES,
    data
})


export const fetchTreeTokenPresent = () => ({
    type: FETCH_TREE
})


export const reInit = () => ({type: RE_INIT})
