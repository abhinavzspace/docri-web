/**
 * Created by abhinav on 23/12/16.
 */

import {fromJS, Map, List, OrderedMap} from 'immutable'

import {REPLACE_ENTRIES, CHANGE_ENTRY, CHANGE_SELECTED_ITEM, SET_DOCRIES
    , DOCRI, FOLDER, NOTE, USERNAME} from './constants'
import {getItemPosKey} from './selectors'

// Get new OrderedMap
function getOm(arr) {
    return OrderedMap().withMutations(map => {
        arr.forEach(item => map.set(item.id, item))
    })
}

// ------------------------------------
// Action Handlers and Reducer
// ------------------------------------
const ACTION_HANDLERS = {

    [REPLACE_ENTRIES]: (state, action) => {
        return state
            .set(action.key, fromJS(action.entriesContainer))
    },

    [SET_DOCRIES]: (state, action) => {
        return state
            .set('docries', fromJS(action.data))
    },

    [CHANGE_SELECTED_ITEM]: (state, action) => {
        // Get position of the item from its id.
        switch (action.itemType) {
            case DOCRI:
                const newDocriPos = state.get('docries').findIndex(docri => docri.get('id') === action.itemId)
                if (newDocriPos === null) throw 'docri not found'
                return state.set('selectedDocriPos', newDocriPos)
                    .set('selectedFolderPos', 0)
                    .set('selectedNotePos', 0)

            case FOLDER:
                let newFolderPos = null
                state.get('docries').forEach(docri => {
                    let folderPos = docri.get('children').findIndex(folder => folder.get('id') === action.itemId)
                    newFolderPos = folderPos >= 0 ? folderPos : newFolderPos
                })
                if (newFolderPos === null) throw 'folder not found'
                return state.set('selectedFolderPos', newFolderPos)
                    .set('selectedNotePos', 0)

            case NOTE:
                let newNotePos = null
                state.get('docries').forEach(docri => {
                    docri.get('children').forEach(folder => {
                        let notePos = folder.get('children').findIndex(note => note.get('id') === action.itemId)
                        newNotePos = notePos >= 0 ? notePos : newNotePos
                    })
                })
                if (newNotePos === null) throw 'note not found'
                return state.set('selectedNotePos', newNotePos)

            case USERNAME:
                return state

            default:
                throw new Error(action.itemType, 'this itemType not available.')
        }
        return state
    },

}

const initialState = fromJS({
    selectedDocriPos: 0,
    selectedFolderPos: 0,
    selectedNotePos: 0,
    docries: [
        {
            "type": "d",
            "name": "the docri",
            "is_public": false,
            "id": "f53f38f0-b4b9-42da-985f-fb9a65a47b49",
            "children": [
                {
                    "type": "f",
                    "name": "another the folder",
                    "is_public": false,
                    "id": "6e35c118-36ab-4e4a-9674-aa12244d3612",
                    "children": []
                },
                {
                    "type": "f",
                    "name": "the folder",
                    "is_public": false,
                    "id": "516d6866-9fff-4c9a-b767-7e987ea5207a",
                    "children": [
                        {
                            "type": "n",
                            "name": "the note",
                            "is_public": false,
                            "id": "6db10f6f-c2ab-47f8-aefa-44f0655680d1"
                        }
                    ]
                }
            ]
        },
        {
            "type": "d",
            "name": "docri x bobobobo",
            "is_public": false,
            "id": "7f75dabb-1cd2-4e54-866e-1fe739a97647",
            "children": []
        },
        {
            "type": "d",
            "name": "docri x yoooo",
            "is_public": false,
            "id": "e19b5408-57cf-4f1c-94d0-2e8bf2d618ef",
            "children": []
        }
    ]
})
function itemsReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler ? handler(state, action) : state
}


export default itemsReducer
