/**
 * Created by abhinav on 23/12/16.
 */
import conf from '../../../../../conf-abhinav'
import {Observable} from 'rxjs'
import {combineEpics} from 'redux-observable'
import fetch from 'isomorphic-fetch'
import {getUsername, getToken} from '../utils/connectors'
import {CREATE_ENTRY, ITEM_ROUTE_CHANGE, FETCH_TREE, SET_DOCRIES} from './constants'
import {USERNAME, DOCRI, FOLDER, NOTE, DOCRI_COLL} from '../DEPRECATED/Reaction/constants'
import {selectSelectedItem} from './selectors'
// import {reaction} from '../Reaction'
// import {createList} from '../DEPRECATED/deepstream'
import {selectRouteParamsFromState} from '../App/selectors'
import {changeSelectedItem, setDocries} from './actionCreators'
import {authLogin} from '../Auth/actionCreators'

const authEndpoint = conf.authEndpoint

const createItem = (data) =>
    Observable.create(o => {
        const {action, state} = data
        const {name, itemType} = action
        // const ds = getDs()
        const username = getUsername()
        // const itemsState = selectItemsState(state)

        let dataToSend
        let to
        switch (itemType) {
            case DOCRI:
                dataToSend = {
                    type: 'd',
                    parent_id: null,
                    name
                }
                break

            case FOLDER:
                const docri_id = selectSelectedItem(DOCRI)(state).id
                dataToSend = {
                    type: 'f',
                    parent_id: docri_id,
                    public_perm: 'private',
                    name
                }
                break

            case NOTE:
                const folder_id = selectSelectedItem(FOLDER)(state).id
                dataToSend = {
                    type: 'n',
                    parent_id: folder_id,
                    name
                }
                break

            default:
                throw new Error('This itemType is wrong.', {itemType})
        }

        let myHeaders = new Headers()
        myHeaders.append('Content-Type', 'application/json');
        myHeaders.append('Authorization', 'Bearer ' + getToken())
        fetch(`${authEndpoint}/items`, {
            method: 'POST',
            headers: myHeaders,
            mode: 'cors',
            cache: 'default',
            body: JSON.stringify(dataToSend)
        })
            .then(response => response.json())
            // .then(json => json.jwt)
            .then(() => {
                // TODO: DON'T LOAD THE WHOLE TREE AGAIN
                o.next({type: FETCH_TREE});
                o.complete();
            }).catch(err => o.error(err))

        return () => {
        }
    })


export const createItemEpic = (action$, {getState}) =>
    action$.ofType(CREATE_ENTRY)
        .map(action => ({action, state: getState()}))
        .mergeMap(data =>
            createItem(data)
        )


export const routeChangeEpic = (action$, {getState}) =>
    action$.filter(action => action.type === SET_DOCRIES || action.type === ITEM_ROUTE_CHANGE)   // ds must be present
        .mergeMap(() => {
            const urlParams = selectRouteParamsFromState(getState())
            console.log('routeChangeEpic', {urlParams})
            const {username, item, id} = urlParams.urlParams
            return Observable.of(changeSelectedItem(item, id))
            // return doReaction(username, item, id, getState)
            // return Observable.empty()
        })


const fetchDocriesTree = () => {
    return Observable.create(o => {
        let myHeaders = new Headers()
        myHeaders.append('Authorization', 'Bearer ' + getToken())
        fetch(`${authEndpoint}/docri-tree`, {
            headers: myHeaders,
            mode: 'cors',
        })
            .then(response => response.json())
            .then(json => json.data)
            .then(data => {
                console.log('tree fetched', data)
                o.next(data)
                o.complete()
            }).catch(err => o.error(err))
    })

}


export const fetchDocriesTreeEpic = (action$, {getState}) =>
    action$.ofType(FETCH_TREE)
        .switchMap(() =>
            fetchDocriesTree()
                .mergeMap(data =>
                    Observable.merge(
                        Observable.of(setDocries(data)),        // dispatch
                        Observable.of(authLogin())              // dispatch
                    )
                )
        )


export default combineEpics(createItemEpic, routeChangeEpic, fetchDocriesTreeEpic)
