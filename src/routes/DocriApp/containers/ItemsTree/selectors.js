/**
 * Created by abhinav on 23/12/16.
 */

import {createSelector} from 'reselect'
import {DOCRI, FOLDER, NOTE} from './constants'

export const getItemPosKey = (type) => {
    switch (type) {
        case DOCRI:
            return 'selectedDocriPos'
        case FOLDER:
            return 'selectedFolderPos'
        case NOTE:
            return 'selectedNotePos'
        default:
            throw type + ' - itemType is not possible.'
    }
}

export const selectItemsState = (state) => state.getIn(['user', 'items'])

export const selectedItemPos = (type) => (state) => {
    const itemsState = state.getIn(['user', 'items'])

    const key = getItemPosKey(type)
    return itemsState.get(key)
}

export const selectItems = (type) => (state) => {
    const itemsState = state.getIn(['user', 'items'])

    let docriPos
    switch (type) {
        case DOCRI:
            const docries = itemsState.get('docries')
            return docries ? docries.toJS() : []

        case FOLDER:
            docriPos = itemsState.get(getItemPosKey(DOCRI))
            if (Number.isInteger(docriPos)) {
                const folders = itemsState.getIn(['docries', docriPos, 'children'])
                return folders ? folders.toJS() : []
            }
            return []

        case NOTE:
            docriPos = itemsState.get(getItemPosKey(DOCRI))
            const folderPos = itemsState.get(getItemPosKey(FOLDER))
            if (Number.isInteger(docriPos) && Number.isInteger(folderPos)) {
                const notes = itemsState.getIn(['docries', docriPos, 'children', folderPos, 'children'])
                return notes ? notes.toJS() : []
            }
            return []

        default:
            throw type + ' - itemType is not possible.'
    }
}

export const selectSelectedItem = (type) => (state) => {
    const items = selectItems(type)(state)
    const itemPos = selectedItemPos(type)(state)

    return items[itemPos] || {}
}


// by reducerKey
// export const selectSelectedItem = (baseType) => createSelector(
//     selectItemsState,
//     (itemsState) => {
//         const reducerKey = getReducerKey(baseType)
//         return itemsState.get(reducerKey).toJS()
//     }
// )

