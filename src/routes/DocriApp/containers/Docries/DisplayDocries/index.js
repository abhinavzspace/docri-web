/**
 * Created by abhinav on 13/10/16.
 */
import React, {PropTypes} from 'react'
import {createStructuredSelector} from 'reselect'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap'
import {getUsername} from '../../utils/connectors'
import {selectSelectedItem, selectItems} from '../../ItemsTree/selectors'
import {DOCRI} from '../../ItemsTree/constants'

const DisplayDocries = ({items, selectedItem, router}) => {
    let docriChange = (e) => {
        const id = e.target.value
        router.push(`/${getUsername()}/docri/${id}`)
    }
    const displayDocries = () => {
        let docriesOptions = []
        for (let item of items) {
            docriesOptions.push(
                <option value={item.id} key={item.id}>
                    {item.name}
                </option>
            )
        }
        return docriesOptions
    }
    return (
        <div>
            <FormGroup controlId="formControlsSelect" style={{width: 200+'px'}}>
                {/*<ControlLabel>Select Docri</ControlLabel>*/}
                <FormControl componentClass="select"
                             placeholder="select"
                             onChange={docriChange}
                             value={selectedItem && selectedItem.id}>
                    {displayDocries()}
                </FormControl>
            </FormGroup>
        </div>
    )
}

DisplayDocries.propTypes = {
    items: PropTypes.array.isRequired,
    selectedItem: PropTypes.object,
    router: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired
}

const mapStateToProps = createStructuredSelector({
    selectedItem: selectSelectedItem(DOCRI),
    items: selectItems(DOCRI)
})

export default withRouter(connect(mapStateToProps)(DisplayDocries))
// export default DisplayDocries
