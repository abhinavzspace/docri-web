/**
 * Created by abhinav on 10/12/16.
 */
import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap'
import {DOCRI} from '../../ItemsTree/constants'
import {createEntryFor} from '../../ItemsTree/actionCreators'
import {createStructuredSelector} from 'reselect'

const CreateDocri = ({createDocri}) => {
    let input
    const submit = e => {
        e.preventDefault()
        if (!input.value.trim()) {
            return
        }
        createDocri(input.value)
        input.value = ''
    }
    return (
        <div>
            <form className="form-inline" onSubmit={submit}>
                <div className="form-group form-group-sm">
                    <input type="text" className="form-control" id="docri-name" ref={node => {
                        input = node
                    }} placeholder="Docri Name"/>
                </div>
                <button type="submit" className="btn btn-default btn-s">Create Docri</button>
            </form>
        </div>
    )
}

CreateDocri.propTypes = {
    createDocri: PropTypes.func.isRequired
}

const mapDispatchToProps = {
    createDocri: createEntryFor(DOCRI),
}

const mapStateToProps = createStructuredSelector({
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateDocri)
// export default CreateDocri
