// TODO: not the best way

let conf

if (__DEV__) {
    conf = {
        "authEndpoint": "http://localhost:4000/rest-api",
        "deepstreamEndpoint": "localhost:6020",
        // "prosemirrorEndpoint": "http://localhost:5555"
        "prosemirrorEndpoint": "http://localhost:5000"
    }
    // conf = {
    //     "authEndpoint": "http://130.211.253.119",
    //     "deepstreamEndpoint": "104.199.230.156",
    //     "prosemirrorEndpoint": "http://104.199.243.62",
    //     "annotatorEndpoint": "http://104.199.244.247"
    // }
} else if (__PROD__) {
    conf = {
        "authEndpoint": "http://130.211.253.119",
        "deepstreamEndpoint": "104.199.230.156",
        "prosemirrorEndpoint": "http://104.199.243.62",
        "annotatorEndpoint": "http://104.199.244.247"
    }
}

export default conf
